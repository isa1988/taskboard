﻿using Interface.Dto;
using Interface.Filters;

namespace Interface.Helper
{
    public static class ClearSpaceExtensions
    {
        public static List<T>? ClearSpacesInListOfDto<T>(this List<T>? list) where T : IDtoGeneral
        {
            if (list == null || list.Count == 0)
            {
                return list;
            }
            foreach (T item in list)
            {
                item.ClearSpace();
            }
            return list; 
        }

        public static T? ClearSpacesOfDto<T>(this T? dto) where T : IDtoGeneral
        {
            if (dto == null)
            {
                return dto;
            }

            dto.ClearSpace();
            return dto;
        }

        public static List<T>? ClearSpacesInListOfFilterDto<T>(this List<T>? list) where T : IFilterDto
        {
            if (list == null || list.Count == 0)
            {
                return list;
            }
            foreach (T item in list)
            {
                item.ClearSpace();
            }
            return list;
        }

        public static T? ClearSpacesOfFilterDto<T>(this T? dto) where T : IFilterDto
        {
            if (dto == null)
            {
                return dto;
            }

            dto.ClearSpace();
            return dto;
        }
    }
}
