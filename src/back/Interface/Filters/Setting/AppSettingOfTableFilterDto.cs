﻿namespace Interface.Filters.Setting
{
    public record AppSettingOfTableFilterDto : IFilterDto
    {
        public Guid AppSettingId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
