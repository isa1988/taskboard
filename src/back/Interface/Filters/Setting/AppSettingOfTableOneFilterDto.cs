﻿using Primitive.AppSetting;

namespace Interface.Filters.Setting
{
    public record AppSettingOfTableOneFilterDto : IFilterDto
    {
        public Guid AppSettingId { get; set; }
        public AppSettingOfTableType Type { get; set; }
        public void ClearSpace()
        {

        }
    }
}
