﻿namespace Interface.Filters.Setting
{
    public record AppSettingOneFilterDto : IFilterDto
    {
        public string Name { get; set; } = string.Empty;
        public void ClearSpace()
        {
            Name = Name.Trim();
        }
    }
}
