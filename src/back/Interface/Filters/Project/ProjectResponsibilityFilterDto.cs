﻿using Primitive.Helper;

namespace Interface.Filters.Project
{
    public record ProjectResponsibilityFilterDto : IFilterDto
    {
        public string? Name { get; set; }
        public Guid ProjectId { get; set; }

        public void ClearSpace()
        {
            Name = Name.TrimWithNull();
        }
    }
}
