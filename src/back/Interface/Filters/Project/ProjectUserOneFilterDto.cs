﻿namespace Interface.Filters.Project
{
    public record ProjectUserOneFilterDto : IFilterDto
    {
        public Guid ProjectId { get; set; }
        public Guid UserId { get; set; }
        public void ClearSpace()
        {
            
        }
    }
}
