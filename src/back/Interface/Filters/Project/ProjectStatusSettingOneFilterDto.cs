﻿using Primitive.Project;

namespace Interface.Filters.Project
{
    public record ProjectStatusSettingOneFilterDto : IFilterDto
    {
        public Guid ProjectStatusId { get; set; }
        public ProjectStatusSettingEnum Status { get; set; }
        public void ClearSpace()
        {

        }
    }
}
