﻿namespace Interface.Filters.Project
{
    public record ProjectStatusSettingFilterDto : IFilterDto
    {
        public Guid ProjectStatusId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
