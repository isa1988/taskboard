﻿namespace Interface.Filters.Project
{
    public record ProjectStatusFilterDto : IFilterDto
    {
        public string? Name { get; set; }
        public Guid ProjectId { get; set; }

        public void ClearSpace()
        {
            
        }
    }
}
