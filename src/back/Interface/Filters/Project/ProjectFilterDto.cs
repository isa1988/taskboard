﻿using Primitive.Helper;

namespace Interface.Filters.Project
{
    public record ProjectFilterDto : IFilterDto
    {
        public string? Name { get; set; }
        public Guid? UserId { get; set; }

        public void ClearSpace()
        {
            Name = Name.TrimWithNull();
        }
    }
}
