﻿using Primitive.Project;

namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskUserForTaskFilterDto : IFilterDto
    {
        public Guid? UserId { get; set; }
        public ProjectTaskUserType? Type { get; set; }
        public void ClearSpace()
        {

        }
    }
}
