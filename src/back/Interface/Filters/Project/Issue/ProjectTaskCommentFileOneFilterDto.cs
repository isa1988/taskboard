﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskCommentFileOneFilterDto : IFilterDto
    {
        public Guid ProjectTaskCommentId { get; set; }
        public Guid FileAreaId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
