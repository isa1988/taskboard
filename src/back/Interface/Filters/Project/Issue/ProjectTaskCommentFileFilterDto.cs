﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskCommentFileFilterDto : IFilterDto
    {
        public Guid ProjectTaskCommentId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
