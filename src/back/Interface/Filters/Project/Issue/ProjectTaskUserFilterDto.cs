﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskUserFilterDto
    {
        public Guid ProjectTaskId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
