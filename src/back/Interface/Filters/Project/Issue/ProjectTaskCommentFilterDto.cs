﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskCommentFilterDto : IFilterDto
    {
        public Guid ProjectTaskId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
