﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskResponsibilityOneFilterDto : IFilterDto
    {
        public Guid ProjectResponsibilityId { get; set; }
        public Guid ProjectTaskId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
