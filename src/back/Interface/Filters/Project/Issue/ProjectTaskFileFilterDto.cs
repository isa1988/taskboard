﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskFileFilterDto : IFilterDto
    {
        public Guid ProjectTaskId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
