﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskUserOneFilterDto : IFilterDto
    {
        public Guid ProjectTaskId { get; set; }
        public Guid UserId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
