﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskResponsibilityFilterDto : IFilterDto
    {
        public Guid ProjectTaskId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
