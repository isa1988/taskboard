﻿namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskFileOneFilterDto : IFilterDto
    {
        public Guid ProjectTaskId { get; set; }
        public Guid FileAreaId { get; set; }
        public void ClearSpace()
        {

        }
    }
}
