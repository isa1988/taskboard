﻿using Interface.Helper;
using Primitive.Helper;

namespace Interface.Filters.Project.Issue
{
    public record ProjectTaskFilterDto : IFilterDto
    {
        public string? Name { get; set; }
        public long? Number { get; set; }
        public Guid ProjectId { get; set; }
        public Guid? ProjectStatusId { get; set; }
        public List<ProjectTaskUserForTaskFilterDto> ProjectTaskUserList { get; set; } = new List<ProjectTaskUserForTaskFilterDto>();
        public void ClearSpace()
        {
            Name = Name.TrimWithNull();
            ProjectTaskUserList.ClearSpacesInListOfFilterDto();
        }
}
}
