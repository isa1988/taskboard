﻿namespace Interface.Filters.Project
{
    public record ProjectUserFilterDto : IFilterDto
    {
        public Guid ProjectId { get; set; }
        public void ClearSpace() 
        {
            
        }
    }
}
