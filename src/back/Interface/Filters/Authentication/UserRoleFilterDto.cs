﻿namespace Interface.Filters.Authentication
{
    public record UserRoleFilterDto : IFilterDto
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }

        public void ClearSpace()
        {
            
        }
    }
}
