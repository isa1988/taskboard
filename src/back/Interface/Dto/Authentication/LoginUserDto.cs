﻿namespace Interface.Dto.Authentication
{
    public record LoginUserDto : IDtoGeneral
    {
        public string Login { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public void ClearSpace()
        {
            Login = Login.Trim();
            Login = Login.Trim();
        }
    }
}
