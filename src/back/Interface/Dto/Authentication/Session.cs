﻿using Interface.Helper;
using Primitive.Auth;
using System.ComponentModel.DataAnnotations.Schema;

namespace Interface.Dto.Authentication
{
    public class Session : IDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid UserId { get; set; }
        public UserDto? User { get; set; }
        public Guid? RoleId { get; set; }
        [ForeignKey(nameof(RoleId))]
        public RoleDto? Role { get; set; }
        public string Token { get; set; } = string.Empty;
        public SessionStatus Status { get; set; }
        public DateTime CloseDate { get; set; }

        public void ClearSpace()
        {
            Token = Token.Trim();
            User.ClearSpacesOfDto();
            Role.ClearSpacesOfDto();
        }
    }
}
