﻿using Interface.Helper;

namespace Interface.Dto.Authentication
{
    public class UserRoleDto : IDto
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid RoleId { get; set; }
        public RoleDto? Role { get; set; }
        public Guid UserId { get; set; }
        public UserDto? User { get; set; }

        public void ClearSpace()
        {
            Role.ClearSpacesOfDto();
            User.ClearSpacesOfDto();
        }
    }
}
