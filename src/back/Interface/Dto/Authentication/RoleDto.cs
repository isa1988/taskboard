﻿using Interface.Helper;
using Primitive;

namespace Interface.Dto.Authentication
{
    public class RoleDto : IDtoForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public Deleted IsDeleted { get; set; }
        public List<UserRoleDto> UserRoleList { get; set; } = new List<UserRoleDto>();
        public void ClearSpace()
        {
            Name = Name.Trim();
            UserRoleList.ClearSpacesInListOfDto();
        }
    }
}
