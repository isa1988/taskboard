﻿using Interface.Helper;
using Primitive;

namespace Interface.Dto.Authentication
{
    public class UserDto : IDtoForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string FirstName { get; set; } = string.Empty;
        public string MiddleName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Login { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public Deleted IsDeleted { get; set; }
        public List<UserRoleDto> RoleList { get; set; } = new List<UserRoleDto>();
        public List<Session> SessionList { get; set; } = new List<Session>();

        public void ClearSpace()
        {
            FirstName = FirstName.Trim();
            LastName = LastName.Trim();
            Email = Email.Trim();
            Login = Login.Trim();

            RoleList.ClearSpacesInListOfDto();
            SessionList.ClearSpacesInListOfDto();
        }
    }
}
