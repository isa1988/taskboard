﻿using Interface.Helper;
using Primitive.AppSetting;

namespace Interface.Dto.Setting
{
    public record AppSettingOfTableDto : IDto
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid AppSettingId { get; set; }
        public AppSettingDto? AppSetting { get; set; }
        public AppSettingOfTableType Type { get; set; }
        public string Value { get; set; } = string.Empty;
        public string ValueJson { get; set; } = string.Empty;
        public Guid ExternalEntityId { get; set; }

        public void ClearSpace()
        {
            Value = Value.Trim();
            ValueJson = ValueJson.Trim();

            AppSetting.ClearSpacesOfDto();
        }
    }
}
