﻿using Interface.Helper;
using Primitive.AppSetting;

namespace Interface.Dto.Setting
{
    public record AppSettingDto : IDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public AppSettingType Type { get; set; }
        public List<AppSettingOfTableDto> AppSettingOfTableList { get; set; } = new List<AppSettingOfTableDto>();

        public void ClearSpace()
        {
            Name = Name.Trim();
            AppSettingOfTableList.ClearSpacesInListOfDto();
        }
    }
}
