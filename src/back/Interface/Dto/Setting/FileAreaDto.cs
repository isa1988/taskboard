﻿using Primitive;

namespace Interface.Dto.Setting
{
    public record FileAreaDto : IDtoForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Path { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;
        public string Desc { get; set; } = string.Empty;
        public Deleted IsDeleted { get; set; }

        public void ClearSpace()
        {
            Name = Name.Trim();
            Path = Path.Trim();
            Type = Type.Trim();
            Desc = Desc.Trim();
        }
    }
}
