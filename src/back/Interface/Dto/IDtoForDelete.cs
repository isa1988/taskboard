﻿using Primitive;

namespace Interface.Dto
{
    public interface IDtoForDelete : IDto
    {
        Deleted IsDeleted { get; set; }
    }

    public interface IDtoForDelete<TId> : IDtoForDelete
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
