﻿using Interface.Helper;

namespace Interface.Dto.Permission
{
    public class PermissionControllerDto : IDto<Guid>
    {
        public Guid Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public string Name { get; set; } = string.Empty;
        public PermissionDto? Permission { get; set; }
        public Guid PermissionId { get; set; }

        public List<PermissionActionDto> PermissionActionList { get; set; } = new List<PermissionActionDto>();

        public void ClearSpace()
        {
            Name = Name.Trim();
            Permission.ClearSpacesOfDto();

            PermissionActionList.ClearSpacesInListOfDto();
        }

    }
}
