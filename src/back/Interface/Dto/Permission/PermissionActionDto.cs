﻿using Interface.Helper;

namespace Interface.Dto.Permission
{
    public class PermissionActionDto : IDto<Guid>
    {
        public Guid Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public string Title { get; set; } = string.Empty;
        public string Action { get; set; } = string.Empty;

        public PermissionControllerDto? PermissionController { get; set; }
        public Guid PermissionControllerId { get; set; }

        public void ClearSpace()
        {
            Title = Title.Trim();
            Action = Action.Trim();

            PermissionController.ClearSpacesOfDto();
        }
    }
}
