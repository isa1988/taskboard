﻿using Interface.Dto.Authentication;
using Interface.Helper;
using Primitive;

namespace Interface.Dto.Permission
{
    public class PermissionRoleDto : IDto
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public Guid RoleId { get; set; }
        public RoleDto? Role { get; set; }

        public PermissionDto? Permission { get; set; }
        public Guid PermissionId { get; set; }

        public PermissionRoleStatus Status { get; set; }

        public void ClearSpace()
        {
            Role.ClearSpacesOfDto();
            Permission.ClearSpacesOfDto();
        }
    }
}
