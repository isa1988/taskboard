﻿using Interface.Helper;

namespace Interface.Dto.Permission
{
    public class PermissionDto : IDto<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public List<PermissionControllerDto> PermissionControllerList { get; set; } = new List<PermissionControllerDto>();
        public List<PermissionRoleDto> PermissionRoleList { get; set; } = new List<PermissionRoleDto>();

        public void ClearSpace()
        {
            Name = Name.Trim();

            PermissionControllerList.ClearSpacesInListOfDto();
            PermissionRoleList.ClearSpacesInListOfDto();
        }

    }
}
