﻿using Interface.Helper;
using Primitive;

namespace Interface.Dto.Project
{
    public class ProjectStatusDto : IDtoForDelete<Guid>
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public ProjectDto? Project { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int Order { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<ProjectStatusSettingDto> ProjectStatusSettingList { get; set; } = new List<ProjectStatusSettingDto>();

        public void ClearSpace()
        {
            Name = Name.Trim();
            Description = Description.Trim();

            Project.ClearSpacesOfDto();
            ProjectStatusSettingList.ClearSpacesInListOfDto();
        }
    }
}
