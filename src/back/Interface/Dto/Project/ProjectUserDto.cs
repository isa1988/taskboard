﻿using Interface.Dto.Authentication;
using Interface.Helper;

namespace Interface.Dto.Project
{
    public class ProjectUserDto : IDto
    {
        public Guid ProjectId { get; set; }
        public ProjectDto? Project { get; set; }
        public Guid UserId { get; set; }
        public UserDto? User { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public void ClearSpace() 
        {
            Project.ClearSpacesOfDto();
            User.ClearSpacesOfDto();
        }
    }
}
