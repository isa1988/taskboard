﻿using Interface.Helper;
using Primitive;

namespace Interface.Dto.Project
{
    public class ProjectResponsibilityDto : IDtoForDelete<Guid>
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public ProjectDto? Project { get; set; }
        public string Name { get; set; } = string.Empty;
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }

        public void ClearSpace()
        {
            Name = Name.Trim();
            Project.ClearSpacesOfDto();
        }
    }
}
