﻿using Interface.Helper;
using Primitive.Project;

namespace Interface.Dto.Project
{
    public class ProjectStatusSettingDto : IDto
    {
        public Guid ProjectStatusId { get; set; }
        public ProjectStatusDto? ProjectStatus { get; set; }
        public ProjectStatusSettingEnum Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public void ClearSpace() 
        {
            ProjectStatus.ClearSpacesOfDto();
        }
    }
}
