﻿using Interface.Helper;

namespace Interface.Dto.Project.Issue
{
    public class ProjectTaskResponsibilityDto : IDto
    {
        public Guid ProjectResponsibilityId { get; set; }
        public ProjectResponsibilityDto? ProjectResponsibility { get; set; }
        public Guid ProjectTaskId { get; set; }
        public ProjectTaskDto? ProjectTask { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public void ClearSpace()
        {
            ProjectResponsibility.ClearSpacesOfDto();
            ProjectTask.ClearSpacesOfDto();
        }
    }
}
