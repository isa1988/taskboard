﻿using Interface.Dto.Authentication;
using Interface.Helper;

namespace Interface.Dto.Project.Issue
{
    public class ProjectTaskCommentDto : IDto<Guid>
    {
        public Guid Id { get; set; }
        public Guid ProjectTaskId { get; set; }
        public ProjectTaskDto? ProjectTask { get; set; }
        public string Name { get; set; } = string.Empty;
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid UserId { get; set; }
        public UserDto? User { get; set; }
        public List<ProjectTaskCommentFileDto> ProjectTaskCommentFileList { get; set; } = new List<ProjectTaskCommentFileDto>();

        public void ClearSpace()
        {
            Name = Name.Trim();

            ProjectTask.ClearSpacesOfDto();
            User.ClearSpacesOfDto();
            ProjectTaskCommentFileList.ClearSpacesInListOfDto();
        }
    }
}
