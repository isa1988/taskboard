﻿using Interface.Dto.Setting;
using Interface.Helper;

namespace Interface.Dto.Project.Issue
{
    public class ProjectTaskCommentFileDto : IDto
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public ProjectTaskCommentDto? ProjectTaskComment { get; set; }
        public Guid ProjectTaskCommentId { get; set; }
        public Guid FileAreaId { get; set; }
        public FileAreaDto? FileArea { get; set; }

        public void ClearSpace()
        {
            ProjectTaskComment.ClearSpacesOfDto();
            FileArea.ClearSpacesOfDto();
        }
    }
}
