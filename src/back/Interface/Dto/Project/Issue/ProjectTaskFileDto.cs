﻿using Interface.Dto.Setting;
using Interface.Helper;

namespace Interface.Dto.Project.Issue
{
    public class ProjectTaskFileDto : IDto
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public ProjectTaskDto? ProjectTask { get; set; }
        public Guid ProjectTaskId { get; set; }
        public Guid FileAreaId { get; set; }
        public FileAreaDto? FileArea { get; set; }

        public void ClearSpace()
        {
            ProjectTask.ClearSpacesOfDto();
            FileArea.ClearSpacesOfDto();
        }
    }
}
