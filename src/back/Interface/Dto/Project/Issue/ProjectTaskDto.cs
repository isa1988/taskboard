﻿using Interface.Helper;
using Primitive;

namespace Interface.Dto.Project.Issue
{
    public class ProjectTaskDto : IDtoForDelete<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public long Number { get; set; }
        public string Description { get; set; } = string.Empty;
        public Guid ProjectId { get; set; }
        public ProjectDto? Project { get; set; }
        public Guid ProjectTypeId { get; set; }
        public ProjectTypeDto? ProjectType { get; set; }
        public Guid ProjectStatusId { get; set; }
        public ProjectStatusDto? ProjectStatus { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<ProjectTaskCommentDto> ProjectTaskCommentList { get; set; } = new List<ProjectTaskCommentDto>();
        public List<ProjectTaskFileDto> ProjectTaskFileList { get; set; } = new List<ProjectTaskFileDto>();
        public List<ProjectTaskResponsibilityDto> ProjectTaskResponsibilityList { get; set; } = new List<ProjectTaskResponsibilityDto>();
        public List<ProjectTaskUserDto> ProjectTaskUserList { get; set; } = new List<ProjectTaskUserDto>();

        public void ClearSpace()
        {
            Name = Name.Trim();
            Description = Description.Trim();
            
            Project.ClearSpacesOfDto();
            ProjectType.ClearSpacesOfDto();
            ProjectStatus.ClearSpacesOfDto();

            ProjectTaskCommentList.ClearSpacesInListOfDto();
            ProjectTaskFileList.ClearSpacesInListOfDto();
            ProjectTaskResponsibilityList.ClearSpacesInListOfDto();
            ProjectTaskUserList.ClearSpacesInListOfDto();
        }
    }
}
