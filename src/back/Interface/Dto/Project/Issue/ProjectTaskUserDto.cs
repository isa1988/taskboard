﻿using Interface.Dto.Authentication;
using Interface.Helper;
using Primitive.Project;

namespace Interface.Dto.Project.Issue
{
    public class ProjectTaskUserDto : IDto
    {
        public Guid ProjectTaskId { get; set; }
        public ProjectTaskDto? ProjectTask { get; set; }
        public Guid UserId { get; set; }
        public UserDto? User { get; set; }
        public ProjectTaskUserType Type { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public void ClearSpace()
        {
            ProjectTask.ClearSpacesOfDto();
            User.ClearSpacesOfDto();
        }
    }
}
