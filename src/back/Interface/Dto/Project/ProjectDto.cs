﻿using Interface.Dto.Project.Issue;
using Interface.Helper;
using Primitive;

namespace Interface.Dto.Project
{
    public class ProjectDto : IDtoForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Prefix { get; set; } = string.Empty;
        public Deleted IsDeleted { get; set; }
        public List<ProjectTaskDto> ProjectTaskList { get; set; } = new List<ProjectTaskDto>();
        public List<ProjectStatusDto> ProjectStatusList { get; set; } = new List<ProjectStatusDto>();
        public List<ProjectTypeDto> ProjectTypeList { get; set; } = new List<ProjectTypeDto>();
        public List<ProjectResponsibilityDto> ProjectResponsibilityList { get; set; } = new List<ProjectResponsibilityDto>();
        public List<ProjectUserDto> ProjectUserList { get; set; } = new List<ProjectUserDto>();

        public void ClearSpace()
        {
            Name = Name.Trim();
            Prefix = Prefix.Trim();

            ProjectTaskList.ClearSpacesInListOfDto();
            ProjectStatusList.ClearSpacesInListOfDto();
            ProjectTypeList.ClearSpacesInListOfDto();
            ProjectResponsibilityList.ClearSpacesInListOfDto();
            ProjectUserList.ClearSpacesInListOfDto();
        }
    }
}
