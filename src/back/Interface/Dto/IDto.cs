﻿namespace Interface.Dto
{
    public interface IDtoGeneral
    {
        void ClearSpace();
    }
    public interface IDto : IDtoGeneral
    {
        DateTime CreateDate { get; set; }
        DateTime ModifyDate { get; set; }
    }

    public interface IDto<TId> : IDto
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
