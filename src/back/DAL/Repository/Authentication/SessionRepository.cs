﻿using Core.Contract.Authentication;
using Core.Entity.Authentication;
using Core.Filters;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.Auth;
using Primitive.MyException;
using Primitive.PagingOrderSettings.Authentication;

namespace DAL.Repository.Authentication
{
    public class SessionRepository : RepositoryGuid<Session, OrderSessionFields>, ISessionRepository
    {
        public SessionRepository(TaskBoardContext contextDB)
            : base(contextDB)
        {
        }

        public async Task<Session> GetActiveByTokenAsync(QueryOptionsOne<string> token)
        {
            var query = ResolveInclude(token).Where(x => x.Token.Trim() == token.Filter.Trim() && x.Status == SessionStatus.Active);
            query = GetQueryTrackingBehavior(query, token);
            var session = await query.FirstOrDefaultAsync();
            if (session == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(session);
            return session;
        }

        public async Task CloseAllOpenSessionAsync(QueryOptionsOne<Guid> userId)
        {
            var sessionList = await ResolveInclude(userId).Where(x => x.UserId == userId.Filter && x.Status == SessionStatus.Active).ToListAsync();
            if (sessionList?.Count > 0)
            {
                foreach (var session in sessionList)
                {
                    session.Status = SessionStatus.Closed;
                    session.CloseDate = DateTime.UtcNow;
                    Update(session);
                }
            }
        }

        public async Task<List<Session>> GetByUserListAsync(QueryOptions<List<Guid>, OrderSessionFields> userListId)
        {
            var query = ResolveInclude(userListId).Where(x => userListId.Filter.Any(uid => x.UserId == uid && x.Status == SessionStatus.Active));
            var sessionList = await GetListAsync(query, userListId);

            return sessionList;
        }

        public async Task<List<Session>> GetAllByUserRoleAsync(QueryOptions<Guid, OrderSessionFields> userRoleId)
        {
            var query = ResolveInclude(userRoleId).Where(x => x.RoleId == userRoleId.Filter);
            
            var entities = await GetListAsync(query, userRoleId);

            return entities;
        }

        public async Task<Guid> GetUserIdByActiveSession(QueryOptionsOne<Guid> sessionId)
        {
            var query = ResolveInclude(sessionId).Where(x => x.Id == sessionId.Filter && x.Status == SessionStatus.Active);
            query = GetQueryTrackingBehavior(query, sessionId);
            var userId = await query.FirstOrDefaultAsync();
            if (userId == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            return userId.UserId;
        }

        protected override void ClearDbSetForInclude(Session entity)
        {

        }

        protected override IQueryable<Session> OrderSort(IQueryable<Session> query, IOrderSetting<OrderSessionFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderSessionFields.None)
                return query;

            return query;
        }

        protected override IQueryable<Session> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<Session> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            if (resolveOptions.ResolveOptions.IsUser)
            {
                query = query.Include(x => x.User);
            }

            if (resolveOptions.ResolveOptions.IsRole)
            {
                query = query.Include(x => x.Role);
            }

            return query;
        }
    }
}
