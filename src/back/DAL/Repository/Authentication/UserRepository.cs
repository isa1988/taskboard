﻿using System.Security.Cryptography;
using System.Text;
using Core.Contract.Authentication;
using Core.Entity.Authentication;
using Core.Filters;
using Core.Filters.Authentication;
using Core.Filters.Authentication.Check;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive;
using Primitive.PagingOrderSettings;
using Primitive.PagingOrderSettings.Authentication;
namespace DAL.Repository.Authentication
{
    public class UserRepository : RepositoryForDeletedGuid<User, OrderUserFields>, IUserRepository
    {
        public UserRepository(TaskBoardContext contextDB)
            : base(contextDB)
        {
        }

        public async Task<List<User>> GetAllFromSearchAsync(QueryOptionsForDelete<string, OrderUserFields> filter)
        {
            var query = ResolveInclude(filter);

            query = query.Where(x => x.FirstName.Contains(filter.Filter) || x.LastName.Contains(filter.Filter));
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllFromSearchCountAsync(QueryOptionsForDeleteCount<string> filter)
        {
            var query = ResolveInclude(filter);

            query = query.Where(x => x.FirstName.Contains(filter.Filter) || x.LastName.Contains(filter.Filter));
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);

            var count = await query.CountAsync();
            return count;
        }

        public async Task<bool> IsEqualEmailAsync(QueryOptionsForDeleteOne<CheckEmailOfUserFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.Email == filter.Filter.Email);
            query = query.Where(x => filter.Filter.Id.HasValue && x.Id != filter.Filter.Id);
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);

            var isEqual = await query.AnyAsync();
            
            return isEqual;
        }

        public async Task<User?> GetUserAsync(QueryOptionsForDeleteOne<LoginUserFilter> filter)
        {
            filter.Filter.Password = HashPassword(filter.Filter.Password);
            var entity = await ResolveInclude(filter).FirstOrDefaultAsync(x => x.IsDeleted == Deleted.UnDeleted &&
                                                                                         x.Email == filter.Filter.Login && x.Password == filter.Filter.Password);

            return entity;
        }

        public override Task<User> AddAsync(User entity)
        {
            entity.Password = HashPassword(entity.Password);
            return base.AddAsync(entity);
        }

        string HashPassword(string password)
        {
            var sha1 = new MD5CryptoServiceProvider();
            var data = Encoding.ASCII.GetBytes(password);
            var sha1data = sha1.ComputeHash(data);

            return Convert.ToBase64String(sha1data);
        }

        public string GetEncryptedPassword(QueryOptionsForDeleteOne<string> password)
        {
            var retVal = HashPassword(password.Filter);
            return retVal;
        }

        protected override void ClearDbSetForInclude(User entity)
        {

        }

        protected override IQueryable<User> OrderSort(IQueryable<User> query, IOrderSetting<OrderUserFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderUserFields.None)
                return query;

            if (pagingOrderSetting.PagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderUserFields.Name:
                        {
                            query = query.OrderBy(x => x.FirstName);
                            break;
                        }
                    case OrderUserFields.Email:
                        {
                            query = query.OrderBy(x => x.Email);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderUserFields.Name:
                        {
                            query = query.OrderByDescending(x => x.FirstName);
                            break;
                        }
                    case OrderUserFields.Email:
                        {
                            query = query.OrderByDescending(x => x.Email);
                            break;
                        }
                }
            }

            return query;
        }

        protected override IQueryable<User> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<User> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }

    }
}
