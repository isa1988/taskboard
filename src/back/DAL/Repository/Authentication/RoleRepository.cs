﻿using Core.Contract.Authentication;
using Core.Entity.Authentication;
using Core.Filters;
using Core.Filters.Authentication.Check;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.MyException;
using Primitive.PagingOrderSettings.Authentication;

namespace DAL.Repository.Authentication
{
    public class RoleRepository : RepositoryForDeletedGuid<Role, OrderRoleFields>, IRoleRepository
    {
        public RoleRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfRoleFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.Name == filter.Filter.Name);
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);
            if (filter.Filter.Id.HasValue)
            {
                query = query.Where(x => x.Id != filter.Filter.Id.Value);
            }
            var isEqual = await query.AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(Role entity)
        {

        }

        protected override IQueryable<Role> OrderSort(IQueryable<Role> query, IOrderSetting<OrderRoleFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderRoleFields.None)
                return query;
            return query;
        }

        protected override IQueryable<Role> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<Role> query = dbSetQueryable;

            return query;
        }
    }
}
