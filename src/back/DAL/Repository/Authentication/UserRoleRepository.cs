﻿using Core.Contract.Authentication;
using Core.Entity.Authentication;
using Core.Filters.Authentication;
using Core.Filters;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.MyException;
using Primitive.PagingOrderSettings.Authentication;

namespace DAL.Repository.Authentication
{
    public class UserRoleRepository : Repository<UserRole, OrderUserRoleFields>, IUserRoleRepository
    {
        public UserRoleRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<UserRole?> GetByRoleForUserAsync(QueryOptionsOne<UserRoleFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.UserId == filter.Filter.UserId && x.RoleId == filter.Filter.RoleId);
            query = GetQueryTrackingBehavior(query, filter);
            var role = await query.FirstOrDefaultAsync();
            return role;
        }

        public async Task<int> GetByUserCountAsync(QueryOptionsCount<Guid> userId)
        {
            var count = await ResolveInclude(userId).Where(x => x.UserId == userId.Filter).CountAsync();
            return count;
        }

        public async Task<List<UserRole>> GetByUserListAsync(QueryOptions<Guid, OrderUserRoleFields> userId)
        {
            var query = ResolveInclude(userId).Where(x => x.UserId == userId.Filter);
            query = GetQueryTrackingBehavior(query, userId);

            var roleList = await GetListAsync(query, userId);
            return roleList;
        }


        protected override void ClearDbSetForInclude(UserRole entity)
        {

        }

        protected override IQueryable<UserRole> OrderSort(IQueryable<UserRole> query, IOrderSetting<OrderUserRoleFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderUserRoleFields.None)
                return query;

            return query;
        }

        protected override IQueryable<UserRole> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<UserRole> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            if (resolveOptions.ResolveOptions.IsUser)
            {
                query = query.Include(x => x.User);
            }

            if (resolveOptions.ResolveOptions.IsRole)
            {
                query = query.Include(x => x.Role);
            }

            return query;
        }
    }
}
