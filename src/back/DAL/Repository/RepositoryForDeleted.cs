﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
using Core.Helper;
using Core.Contract;
using Primitive.PagingOrderSettings;
using Core.Entity;
using DAL.Data;
using Primitive;
using Primitive.MyException;
using Core.Filters;

namespace DAL.Repository
{
    public abstract class RepositoryForDeleted<T, TOrder> : Repository<T, TOrder>, IRepositoryForDeleted<T, TOrder>
        where T : class, IEntityForDelete
        where TOrder : Enum
    {
        public RepositoryForDeleted(TaskBoardContext contextDB)
            : base(contextDB)
        {
        }

        public override async Task<List<T>> GetAllAsync(QueryOptions<T, TOrder> filter)
        {
            var querySetting = new QueryOptionsForDelete<T, TOrder>()
            {
                Filter = filter.Filter,
                PagingOrderSetting = filter.PagingOrderSetting,
                ResolveOptions = filter.ResolveOptions,
                TrackingBehavior = filter.TrackingBehavior,
                SettingSelectForDelete = SettingSelectForDelete.OnlyUnDelete,
            };
            var entities = await GetAllAsync(querySetting);

            return entities;
        }

        public override async Task<int> GetAllCountAsync(QueryOptionsCount<T> filter)
        {
            var querySetting = new QueryOptionsForDeleteCount<T>
            {
                Filter = filter.Filter,
                ResolveOptions = filter.ResolveOptions,
                TrackingBehavior = filter.TrackingBehavior,
                SettingSelectForDelete = SettingSelectForDelete.OnlyUnDelete,
            };
            var count = await GetAllCountAsync(querySetting);
            return count;
        }

        public virtual async Task<List<T>> GetAllAsync(QueryOptionsForDelete<T, TOrder> filter)
        {
            var query = ResolveInclude(filter);
            query = SetQueryForDelete(query, filter);
            var entities = await GetListAsync(query, filter);
            ClearDbSetForInclude(entities);
            return entities;
        }

        public virtual async Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<T> filter)
        {
            var query = ResolveInclude(filter);
            query = SetQueryForDelete(query, filter);

            var count = await query.CountAsync();
            return count;
        }

        public override EntityEntry<T> Delete(T entity)
        {
            if (entity.IsDeleted == Deleted.UnDeleted)
            {
                entity.IsDeleted = Deleted.Deleted;
            }
            else
            {
                entity.IsDeleted = Deleted.UnDeleted;
            }
            EntityEntry<T> entryResult = dbSet.Update(entity);

            return entryResult;
        }

        public override async Task<EntityEntry<T>> DeleteAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                if (entity.IsDeleted == Deleted.UnDeleted)
                {
                    entity.IsDeleted = Deleted.Deleted;
                }
                else
                {
                    entity.IsDeleted = Deleted.UnDeleted;
                }
                return dbSet.Update(entity);
            });
            var entry = await task;
            return entry;
        }

        public EntityEntry<T> DeleteFromDbAsync(T entity)
        {
            return dbSet.Remove(entity);
        }

        protected IQueryable<T> SetQueryForDelete(IQueryable<T> query, IQuerySettingSelectForDelete settingSelectForDelete)
        {
            if (settingSelectForDelete.SettingSelectForDelete == SettingSelectForDelete.OnlyDelete)
            {
                query = query.Where(x => x.IsDeleted == Deleted.Deleted);
            }
            else if (settingSelectForDelete.SettingSelectForDelete == SettingSelectForDelete.OnlyUnDelete)
            {
                query = query.Where(x => x.IsDeleted == Deleted.UnDeleted);
            }

            return query;
        }
    }

    public abstract class RepositoryForDeleted<T, TId, TOrder> : RepositoryForDeleted<T, TOrder>, IRepositoryForDeleted<T, TId, TOrder>
        where T : class, IEntityForDelete<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        public RepositoryForDeleted(TaskBoardContext contextDB)
            : base(contextDB)
        {
        }

        public virtual async Task<T> GetByIdAsync(QueryOptionsOne<TId> filter)
        {
            var querySetting = new QueryOptionsForDeleteOne<TId>
            {
                Filter = filter.Filter,
                ResolveOptions = filter.ResolveOptions,
                TrackingBehavior = filter.TrackingBehavior,
                SettingSelectForDelete = SettingSelectForDelete.OnlyUnDelete,
            };
            var entity = await GetByIdAsync(querySetting);

            return entity;
        }

        public virtual async Task<T> GetByIdAsync(QueryOptionsForDeleteOne<TId> filter)
        {
            var query = ResolveInclude(filter);
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);
            var entity = await query.FirstOrDefaultAsync(x => x.Id.Equals(filter.Filter));
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }
    }

    public abstract class RepositoryForDeletedGuid<T, TOrder> : RepositoryForDeleted<T, Guid, TOrder>, IRepositoryForDeleted<T, Guid, TOrder>
        where T : class, IEntityForDelete<Guid>
        where TOrder : Enum
    {
        public RepositoryForDeletedGuid(TaskBoardContext contextDB)
            : base(contextDB)
        {
        }

        public override async Task<T> GetByIdAsync(QueryOptionsOne<Guid> filter)
        {
            var querySetting = new QueryOptionsForDeleteOne<Guid>
            {
                Filter = filter.Filter,
                ResolveOptions = filter.ResolveOptions,
                TrackingBehavior = filter.TrackingBehavior,
                SettingSelectForDelete = SettingSelectForDelete.OnlyUnDelete,
            };
            var entity = await GetByIdAsync(querySetting);

            return entity;
        }

        public override async Task<T> GetByIdAsync(QueryOptionsForDeleteOne<Guid> filter)
        {
            var query = ResolveInclude(filter);
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);
            query = query.Where(x => x.Id == filter.Filter);
            var entity = await query.FirstOrDefaultAsync();
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }

    }
}
