﻿using Core.Entity.Setting;
using Core.Filters.Setting;
using Core.Filters;
using Primitive.PagingOrderSettings.Setting;
using Core.Helper;
using DAL.Data;
using Core.Contract.Setting;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository.Setting
{
    public class AppSettingOfTableRepository : Repository<AppSettingOfTable, OrderAppSettingOfTableFields>, IAppSettingOfTableRepository
    {
        public AppSettingOfTableRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<AppSettingOfTable>> GetAllAsync(QueryOptions<AppSettingOfTableFilter, OrderAppSettingOfTableFields> filter)
        {
            var query = GetAppSettingOfTableFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<AppSettingOfTableFilter> filter)
        {
            var query = GetAppSettingOfTableFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<AppSettingOfTable> GetAppSettingOfTableFilterQuery(AppSettingOfTableFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.AppSettingId == filter.AppSettingId);
            return query;
        }

        public async Task<AppSettingOfTable?> GetByIdAsync(QueryOptionsForDeleteOne<AppSettingOfTableOneFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.AppSettingId == filter.Filter.AppSettingId && x.Type == filter.Filter.Type);
            query = GetQueryTrackingBehavior(query, filter);

            var entity = await query.FirstOrDefaultAsync();

            return entity;
        }

        protected override void ClearDbSetForInclude(AppSettingOfTable entity)
        {

        }

        protected override IQueryable<AppSettingOfTable> OrderSort(IQueryable<AppSettingOfTable> query, IOrderSetting<OrderAppSettingOfTableFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderAppSettingOfTableFields.None)
                return query;

            return query;
        }

        protected override IQueryable<AppSettingOfTable> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<AppSettingOfTable> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
