﻿using Core.Contract.Setting;
using Core.Entity.Setting;
using Core.Filters;
using Core.Helper;
using DAL.Data;
using Primitive.PagingOrderSettings.Setting;

namespace DAL.Repository.Setting
{
    public class FileAreaRepository : RepositoryForDeletedGuid<FileArea, OrderFileAreaFields>, IFileAreaRepository
    {
        public FileAreaRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        protected override void ClearDbSetForInclude(FileArea entity)
        {
            
        }

        protected override IQueryable<FileArea> OrderSort(IQueryable<FileArea> query, IOrderSetting<OrderFileAreaFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderFileAreaFields.None)
                return query;

            return query;
        }

        protected override IQueryable<FileArea> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<FileArea> query = dbSetQueryable;
            if (resolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
