﻿using Core.Contract.Setting;
using Core.Entity.Setting;
using Core.Filters;
using Core.Filters.Setting;
using Core.Filters.Setting.Check;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Setting;

namespace DAL.Repository.Setting
{
    public class AppSettingRepository : RepositoryGuid<AppSetting, OrderAppSettingFields>, IAppSettingRepository
    {
        public AppSettingRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<AppSetting?> GetByIdAsync(QueryOptionsForDeleteOne<AppSettingOneFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.Name == filter.Filter.Name);
            query = GetQueryTrackingBehavior(query, filter);

            var entity = await query.FirstOrDefaultAsync();

            return entity;
        }

        public async Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfAppSettingFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.Name == filter.Filter.Name);
            query = query.Where(x => filter.Filter.Id.HasValue && x.Id != filter.Filter.Id);
            query = GetQueryTrackingBehavior(query, filter);

            var isEqual = await query.AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(AppSetting entity)
        {
            
        }

        protected override IQueryable<AppSetting> OrderSort(IQueryable<AppSetting> query, IOrderSetting<OrderAppSettingFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderAppSettingFields.None)
                return query;

            return query;
        }

        protected override IQueryable<AppSetting> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<AppSetting> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
