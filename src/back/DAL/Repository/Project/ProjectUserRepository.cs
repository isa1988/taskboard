﻿using Core.Contract.Project;
using Core.Entity.AreaProject;
using Core.Filters;
using Core.Filters.Project;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Project;

namespace DAL.Repository.Project
{
    public class ProjectUserRepository : Repository<ProjectUser, OrderProjectUserFields>, IProjectUserRepository
    {
        public ProjectUserRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<ProjectUser>> GetAllAsync(QueryOptions<ProjectUserFilter, OrderProjectUserFields> filter)
        {
            var query = GetProjectUserFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<ProjectUserFilter> filter)
        {
            var query = GetProjectUserFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectUser> GetProjectUserFilterQuery(ProjectUserFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.ProjectId == filter.ProjectId);

            return query;
        }

        public async Task<bool> GetByIdAsync(QueryOptionsOne<ProjectUserOneFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.ProjectId == filter.Filter.ProjectId && x.UserId == filter.Filter.UserId);
            query = GetQueryTrackingBehavior(query, filter);

            var isEqual = await query.AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(ProjectUser entity)
        {
            
        }

        protected override IQueryable<ProjectUser> OrderSort(IQueryable<ProjectUser> query, IOrderSetting<OrderProjectUserFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectUserFields.None)
                return query;

            return query;
        }

        protected override IQueryable<ProjectUser> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectUser> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
