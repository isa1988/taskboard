﻿using Core.Contract.Project;
using Core.Filters;
using Core.Filters.Project;
using Core.Filters.Project.Check;
using Core.Helper;
using DAL.Data;
using Primitive.PagingOrderSettings;
using Primitive.PagingOrderSettings.Project;
using Microsoft.EntityFrameworkCore;
using Core.Entity.AreaProject;

namespace DAL.Repository.Project
{
    public class ProjectTypeRepository : RepositoryForDeletedGuid<ProjectType, OrderProjectTypeFields>, IProjectTypeRepository
    {
        public ProjectTypeRepository(TaskBoardContext contextDB) 
            : base(contextDB)
        {
        }

        public async Task<List<ProjectType>> GetAllAsync(QueryOptionsForDelete<ProjectTypeFilter, OrderProjectTypeFields> filter)
        {
            var query = GetProjectTypeFilterQuery(filter.Filter, filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectTypeFilter> filter)
        {
            var query = GetProjectTypeFilterQuery(filter.Filter, filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectType> GetProjectTypeFilterQuery(ProjectTypeFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter,
            IQuerySettingSelectForDelete querySettingSelectForDelete)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => !string.IsNullOrWhiteSpace(filter.Name) && x.Name.Contains(filter.Name));
            query = query.Where(x => x.ProjectId == filter.ProjectId);
            query = SetQueryForDelete(query, querySettingSelectForDelete);

            return query;
        }

        public async Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfProjectTypeFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.Name == filter.Filter.Name);
            query = query.Where(x => filter.Filter.Id.HasValue && x.Id != filter.Filter.Id);
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);

            var isEqual = await query.AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(ProjectType entity)
        {

        }

        protected override IQueryable<ProjectType> OrderSort(
            IQueryable<ProjectType> query, 
            IOrderSetting<OrderProjectTypeFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectTypeFields.None)
                return query;

            if (pagingOrderSetting.PagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderProjectTypeFields.Name:
                        {
                            query = query.OrderBy(x => x.Name);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderProjectTypeFields.Name:
                        {
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        }
                }
            }

            return query;
        }

        protected override IQueryable<ProjectType> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectType> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
