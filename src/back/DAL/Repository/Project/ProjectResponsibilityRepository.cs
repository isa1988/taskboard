﻿using Core.Contract.Project;
using Core.Filters;
using Core.Filters.Project;
using Core.Filters.Project.Check;
using Core.Helper;
using DAL.Data;
using Primitive.PagingOrderSettings;
using Primitive.PagingOrderSettings.Project;
using Microsoft.EntityFrameworkCore;
using Core.Entity.AreaProject;

namespace DAL.Repository.Project
{
    public class ProjectResponsibilityRepository : RepositoryForDeletedGuid<ProjectResponsibility, OrderProjectResponsibilityFields>, IProjectResponsibilityRepository
    {
        public ProjectResponsibilityRepository(TaskBoardContext contextDB) 
            : base(contextDB)
        {
        }

        public async Task<List<ProjectResponsibility>> GetAllAsync(QueryOptionsForDelete<ProjectResponsibilityFilter, OrderProjectResponsibilityFields> filter)
        {
            var query = GetProjectResponsibilityFilterQuery(filter.Filter, filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectResponsibilityFilter> filter)
        {
            var query = GetProjectResponsibilityFilterQuery(filter.Filter, filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectResponsibility> GetProjectResponsibilityFilterQuery(ProjectResponsibilityFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter,
            IQuerySettingSelectForDelete querySettingSelectForDelete)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => !string.IsNullOrWhiteSpace(filter.Name) && x.Name.Contains(filter.Name));
            query = query.Where(x => x.ProjectId == filter.ProjectId);
            query = SetQueryForDelete(query, querySettingSelectForDelete);

            return query;
        }

        public async Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfProjectResponsibilityFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.Name == filter.Filter.Name);
            query = query.Where(x => filter.Filter.Id.HasValue && x.Id != filter.Filter.Id);
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);

            var isEqual = await query.AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(ProjectResponsibility entity)
        {

        }

        protected override IQueryable<ProjectResponsibility> OrderSort(
            IQueryable<ProjectResponsibility> query, 
            IOrderSetting<OrderProjectResponsibilityFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectResponsibilityFields.None)
                return query;

            if (pagingOrderSetting.PagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderProjectResponsibilityFields.Name:
                        {
                            query = query.OrderBy(x => x.Name);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderProjectResponsibilityFields.Name:
                        {
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        }
                }
            }

            return query;
        }

        protected override IQueryable<ProjectResponsibility> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectResponsibility> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
