﻿using Core.Contract.Project;
using Core.Filters;
using Core.Filters.Project;
using Core.Filters.Project.Check;
using Core.Helper;
using DAL.Data;
using Primitive.PagingOrderSettings;
using Primitive.PagingOrderSettings.Project;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository.Project
{
    public class ProjectRepository : RepositoryForDeletedGuid<Core.Entity.AreaProject.Project, OrderProjectFields>, IProjectRepository
    {
        public ProjectRepository(TaskBoardContext contextDB) 
            : base(contextDB)
        {
        }

        public async Task<List<Core.Entity.AreaProject.Project>> GetAllAsync(QueryOptionsForDelete<ProjectFilter, OrderProjectFields> filter)
        {
            var query = GetProjectFilterQuery(filter.Filter, filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectFilter> filter)
        {
            var query = GetProjectFilterQuery(filter.Filter, filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<Core.Entity.AreaProject.Project> GetProjectFilterQuery(ProjectFilter filter, 
            QueryOfTrackingBehaviorFilter behaviorFilter, 
            IQuerySettingSelectForDelete querySettingSelectForDelete)
        {
            if (filter.UserId.HasValue && behaviorFilter.ResolveOptions == null)
            {
                behaviorFilter.ResolveOptions = new ResolveOptions()
                {
                    IsProjectUserList = true
                };
            }
            else if (filter.UserId.HasValue && !behaviorFilter.ResolveOptions.IsProjectUserList)
            {
                behaviorFilter.ResolveOptions.IsProjectUserList = true;
            }
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => !string.IsNullOrWhiteSpace(filter.Name) && x.Name.Contains(filter.Name));
            query = query.Where(x => filter.UserId.HasValue && x.ProjectUserList.Any(id => id.UserId == filter.UserId.Value));
            query = SetQueryForDelete(query, querySettingSelectForDelete);
            return query;
        }

        public async Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfProjectFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.Name == filter.Filter.Name);
            query = query.Where(x => filter.Filter.Id.HasValue && x.Id != filter.Filter.Id);
            query = GetQueryTrackingBehavior(query, filter);
            query = SetQueryForDelete(query, filter);

            var isEqual = await query.AnyAsync();

            return isEqual;
        }

        protected override void ClearDbSetForInclude(Core.Entity.AreaProject.Project entity)
        {

        }

        protected override IQueryable<Core.Entity.AreaProject.Project> OrderSort(
            IQueryable<Core.Entity.AreaProject.Project> query, 
            IOrderSetting<OrderProjectFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectFields.None)
                return query;

            if (pagingOrderSetting.PagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderProjectFields.Name:
                        {
                            query = query.OrderBy(x => x.Name);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderProjectFields.Name:
                        {
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        }
                }
            }

            return query;
        }

        protected override IQueryable<Core.Entity.AreaProject.Project> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<Core.Entity.AreaProject.Project> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
