﻿using Core.Contract.Project.Issue;
using Core.Entity.AreaProject.Issue;
using Core.Filters;
using Core.Filters.Project.Issue;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Project.Issue;

namespace DAL.Repository.Project.Issue
{
    public class ProjectTaskCommentFileRepository : Repository<ProjectTaskCommentFile, OrderProjectTaskCommentFileFields>, IProjectTaskCommentFileRepository
    {
        public ProjectTaskCommentFileRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<ProjectTaskCommentFile>> GetAllAsync(QueryOptions<ProjectTaskCommentFileFilter, OrderProjectTaskCommentFileFields> filter)
        {
            var query = GetProjectTaskCommentFileFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskCommentFileFilter> filter)
        {
            var query = GetProjectTaskCommentFileFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectTaskCommentFile> GetProjectTaskCommentFileFilterQuery(ProjectTaskCommentFileFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.ProjectTaskCommentId == filter.ProjectTaskCommentId);

            return query;
        }


        public async Task<ProjectTaskCommentFile?> GetByIdAsync(QueryOptionsOne<ProjectTaskCommentFileOneFilter> filter)
        {
            var query = ResolveInclude(filter);

            query = query.Where(x => x.ProjectTaskCommentId == filter.Filter.ProjectTaskCommentId && x.FileAreaId == filter.Filter.FileAreaId);
            query = GetQueryTrackingBehavior(query, filter);

            var entity = await query.FirstOrDefaultAsync();
            return entity;
        }

        protected override void ClearDbSetForInclude(ProjectTaskCommentFile entity)
        {

        }

        protected override IQueryable<ProjectTaskCommentFile> OrderSort(IQueryable<ProjectTaskCommentFile> query,
            IOrderSetting<OrderProjectTaskCommentFileFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectTaskCommentFileFields.None)
                return query;

            return query;
        }

        protected override IQueryable<ProjectTaskCommentFile> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectTaskCommentFile> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
