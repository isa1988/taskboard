﻿using Core.Contract.Project.Issue;
using Core.Entity.AreaProject.Issue;
using Core.Filters;
using Core.Filters.Project.Issue;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Project.Issue;

namespace DAL.Repository.Project.Issue
{
    public class ProjectTaskUserRepository : Repository<ProjectTaskUser, OrderProjectTaskUserFields>, IProjectTaskUserRepository
    {
        public ProjectTaskUserRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<ProjectTaskUser>> GetAllAsync(QueryOptions<ProjectTaskUserFilter, OrderProjectTaskUserFields> filter)
        {
            var query = GetProjectTaskUserFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskUserFilter> filter)
        {
            var query = GetProjectTaskUserFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectTaskUser> GetProjectTaskUserFilterQuery(ProjectTaskUserFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.ProjectTaskId == filter.ProjectTaskId);

            return query;
        }


        public async Task<ProjectTaskUser?> GetByIdAsync(QueryOptionsOne<ProjectTaskUserOneFilter> filter)
        {
            var query = ResolveInclude(filter);

            query = query.Where(x => x.ProjectTaskId == filter.Filter.ProjectTaskId && x.UserId == filter.Filter.UserId);
            query = GetQueryTrackingBehavior(query, filter);

            var entity = await query.FirstOrDefaultAsync();
            return entity;
        }

        protected override void ClearDbSetForInclude(ProjectTaskUser entity)
        {

        }

        protected override IQueryable<ProjectTaskUser> OrderSort(IQueryable<ProjectTaskUser> query,
            IOrderSetting<OrderProjectTaskUserFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectTaskUserFields.None)
                return query;

            return query;
        }

        protected override IQueryable<ProjectTaskUser> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectTaskUser> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
