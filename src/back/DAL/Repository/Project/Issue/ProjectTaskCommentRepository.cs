﻿using Core.Contract.Project.Issue;
using Core.Entity.AreaProject.Issue;
using Core.Filters;
using Core.Filters.Project.Issue;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Project.Issue;

namespace DAL.Repository.Project.Issue
{
    public class ProjectTaskCommentRepository : RepositoryGuid<ProjectTaskComment, OrderProjectTaskCommentFields>, IProjectTaskCommentRepository
    {
        public ProjectTaskCommentRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<ProjectTaskComment>> GetAllAsync(QueryOptions<ProjectTaskCommentFilter, OrderProjectTaskCommentFields> filter)
        {
            var query = GetProjectTaskCommentFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskCommentFilter> filter)
        {
            var query = GetProjectTaskCommentFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectTaskComment> GetProjectTaskCommentFilterQuery(ProjectTaskCommentFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.ProjectTaskId == filter.ProjectTaskId);

            return query;
        }

        protected override void ClearDbSetForInclude(ProjectTaskComment entity)
        {

        }

        protected override IQueryable<ProjectTaskComment> OrderSort(IQueryable<ProjectTaskComment> query,
            IOrderSetting<OrderProjectTaskCommentFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectTaskCommentFields.None)
                return query;

            return query;
        }

        protected override IQueryable<ProjectTaskComment> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectTaskComment> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
