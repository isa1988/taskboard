﻿using Core.Contract.Project.Issue;
using Core.Entity.AreaProject.Issue;
using Core.Filters;
using Core.Filters.Project.Issue;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Project.Issue;

namespace DAL.Repository.Project.Issue
{
    public class ProjectTaskResponsibilityRepository : Repository<ProjectTaskResponsibility, OrderProjectTaskResponsibilityFields>, IProjectTaskResponsibilityRepository
    {
        public ProjectTaskResponsibilityRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<ProjectTaskResponsibility>> GetAllAsync(QueryOptions<ProjectTaskResponsibilityFilter, OrderProjectTaskResponsibilityFields> filter)
        {
            var query = GetProjectTaskResponsibilityFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskResponsibilityFilter> filter)
        {
            var query = GetProjectTaskResponsibilityFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectTaskResponsibility> GetProjectTaskResponsibilityFilterQuery(ProjectTaskResponsibilityFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.ProjectTaskId == filter.ProjectTaskId);

            return query;
        }


        public async Task<ProjectTaskResponsibility?> GetByIdAsync(QueryOptionsOne<ProjectTaskResponsibilityOneFilter> filter)
        {
            var query = ResolveInclude(filter);

            query = query.Where(x => x.ProjectTaskId == filter.Filter.ProjectTaskId && x.ProjectResponsibilityId == filter.Filter.ProjectResponsibilityId);
            query = GetQueryTrackingBehavior(query, filter);

            var entity = await query.FirstOrDefaultAsync();
            return entity;
        }

        protected override void ClearDbSetForInclude(ProjectTaskResponsibility entity)
        {

        }

        protected override IQueryable<ProjectTaskResponsibility> OrderSort(IQueryable<ProjectTaskResponsibility> query,
            IOrderSetting<OrderProjectTaskResponsibilityFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectTaskResponsibilityFields.None)
                return query;

            return query;
        }

        protected override IQueryable<ProjectTaskResponsibility> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectTaskResponsibility> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
