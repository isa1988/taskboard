﻿using Core.Contract.Project.Issue;
using Core.Entity.AreaProject.Issue;
using Core.Filters;
using Core.Filters.Project.Issue;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Project.Issue;

namespace DAL.Repository.Project.Issue
{
    public class ProjectTaskFileRepository : Repository<ProjectTaskFile, OrderProjectTaskFileFields>, IProjectTaskFileRepository
    {
        public ProjectTaskFileRepository(TaskBoardContext contextDB) : base(contextDB)
        {
        }

        public async Task<List<ProjectTaskFile>> GetAllAsync(QueryOptions<ProjectTaskFileFilter, OrderProjectTaskFileFields> filter)
        {
            var query = GetProjectTaskFileFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskFileFilter> filter)
        {
            var query = GetProjectTaskFileFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectTaskFile> GetProjectTaskFileFilterQuery(ProjectTaskFileFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.ProjectTaskId == filter.ProjectTaskId);

            return query;
        }


        public async Task<ProjectTaskFile?> GetByIdAsync(QueryOptionsOne<ProjectTaskFileOneFilter> filter)
        {
            var query = ResolveInclude(filter);

            query = query.Where(x => x.ProjectTaskId == filter.Filter.ProjectTaskId && x.FileAreaId == filter.Filter.FileAreaId);
            query = GetQueryTrackingBehavior(query, filter);

            var entity = await query.FirstOrDefaultAsync();
            return entity;
        }

        protected override void ClearDbSetForInclude(ProjectTaskFile entity)
        {

        }

        protected override IQueryable<ProjectTaskFile> OrderSort(IQueryable<ProjectTaskFile> query,
            IOrderSetting<OrderProjectTaskFileFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting   .OrderField == OrderProjectTaskFileFields.None)
                return query;

            return query;
        }

        protected override IQueryable<ProjectTaskFile> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectTaskFile> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
