﻿using Core.Contract.Project.Issue;
using Core.Filters;
using Core.Filters.Project.Issue;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings;
using Primitive.PagingOrderSettings.Project.Issue;

namespace DAL.Repository.ProjectTask.Issue
{
    public class ProjectTaskRepository : RepositoryForDeletedGuid<Core.Entity.AreaProject.Issue.ProjectTask, OrderProjectTaskFields>, IProjectTaskRepository
    {
        public ProjectTaskRepository(TaskBoardContext contextDB)
            : base(contextDB)
        {
        }

        public async Task<List<Core.Entity.AreaProject.Issue.ProjectTask>> GetAllAsync(QueryOptionsForDelete<ProjectTaskFilter, OrderProjectTaskFields> filter)
        {
            var query = GetProjectTaskFilterQuery(filter.Filter, filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectTaskFilter> filter)
        {
            var query = GetProjectTaskFilterQuery(filter.Filter, filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<Core.Entity.AreaProject.Issue.ProjectTask> GetProjectTaskFilterQuery(ProjectTaskFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter,
            IQuerySettingSelectForDelete querySettingSelectForDelete)
        {
            if (filter.ProjectTaskUserList?.Count > 0 && behaviorFilter.ResolveOptions == null)
            {
                behaviorFilter.ResolveOptions = new ResolveOptions()
                {
                    IsProjectTaskUserList = true
                };
            }
            else if (filter.ProjectTaskUserList?.Count > 0 && !behaviorFilter.ResolveOptions.IsProjectTaskUserList)
            {
                behaviorFilter.ResolveOptions.IsProjectTaskUserList = true;
            }
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.ProjectId == filter.ProjectId);
            query = query.Where(x => !string.IsNullOrWhiteSpace(filter.Name) && x.Name.Contains(filter.Name));
            if (filter.ProjectTaskUserList?.Count > 0)
            {
                query = query.Where(x => x.ProjectTaskUserList
                                    .Any(id => filter.ProjectTaskUserList
                                    .Any(userId => (userId.UserId.HasValue && id.UserId == userId.UserId) && (userId.Type.HasValue && id.Type == userId.Type))));
            }
            query = query.Where(x => filter.Number.HasValue && x.Number == filter.Number);
            query = query.Where(x => filter.ProjectStatusId.HasValue && x.ProjectStatusId == filter.ProjectStatusId);
            query = SetQueryForDelete(query, querySettingSelectForDelete);
            return query;
        }

        protected override void ClearDbSetForInclude(Core.Entity.AreaProject.Issue.ProjectTask entity)
        {

        }

        protected override IQueryable<Core.Entity.AreaProject.Issue.ProjectTask> OrderSort(
            IQueryable<Core.Entity.AreaProject.Issue.ProjectTask> query,
            IOrderSetting<OrderProjectTaskFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectTaskFields.None)
                return query;

            if (pagingOrderSetting.PagingOrderSetting.OrderDirection == OrderType.ASC)
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderProjectTaskFields.Name:
                        {
                            query = query.OrderBy(x => x.Name);
                            break;
                        }
                }
            }
            else
            {
                switch (pagingOrderSetting.PagingOrderSetting.OrderField)
                {
                    case OrderProjectTaskFields.Name:
                        {
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        }
                }
            }

            return query;
        }

        protected override IQueryable<Core.Entity.AreaProject.Issue.ProjectTask> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<Core.Entity.AreaProject.Issue.ProjectTask> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
