﻿using Core.Contract.Project;
using Core.Entity.AreaProject;
using Core.Filters;
using Core.Filters.Project;
using Core.Helper;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using Primitive.PagingOrderSettings.Project;

namespace DAL.Repository.Project
{
    public class ProjectStatusSettingRepository : Repository<ProjectStatusSetting, OrderProjectStatusSettingFields>, IProjectStatusSettingRepository
    {
        public ProjectStatusSettingRepository(TaskBoardContext contextDB) 
            : base(contextDB)
        {
        }

        public async Task<List<ProjectStatusSetting>> GetAllAsync(QueryOptions<ProjectStatusSettingFilter, OrderProjectStatusSettingFields> filter)
        {
            var query = GetProjectStatusSettingFilterQuery(filter.Filter, filter);
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public async Task<int> GetAllCountAsync(QueryOptionsCount<ProjectStatusSettingFilter> filter)
        {
            var query = GetProjectStatusSettingFilterQuery(filter.Filter, filter);
            var entities = await query.CountAsync();

            return entities;
        }

        private IQueryable<ProjectStatusSetting> GetProjectStatusSettingFilterQuery(ProjectStatusSettingFilter filter,
            QueryOfTrackingBehaviorFilter behaviorFilter)
        {
            var query = ResolveInclude(behaviorFilter);

            query = query.Where(x => x.ProjectStatusId == filter.ProjectStatusId);

            return query;
        }

        public async Task<ProjectStatusSetting?> GetByIdAsync(QueryOptionsOne<ProjectStatusSettingOneFilter> filter)
        {
            var query = ResolveInclude(filter).Where(x => x.ProjectStatusId == filter.Filter.ProjectStatusId && x.Status == filter.Filter.Status);
            query = GetQueryTrackingBehavior(query, filter);

            var entity = await query.FirstOrDefaultAsync();

            return entity;
        }

        protected override void ClearDbSetForInclude(ProjectStatusSetting entity)
        {

        }

        protected override IQueryable<ProjectStatusSetting> OrderSort(
            IQueryable<ProjectStatusSetting> query, 
            IOrderSetting<OrderProjectStatusSettingFields> pagingOrderSetting)
        {
            if (pagingOrderSetting.PagingOrderSetting == null || 
                !pagingOrderSetting.PagingOrderSetting.IsOrder || 
                pagingOrderSetting.PagingOrderSetting.OrderField == OrderProjectStatusSettingFields.None)
                return query;

            return query;
        }

        protected override IQueryable<ProjectStatusSetting> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions)
        {
            IQueryable<ProjectStatusSetting> query = dbSetQueryable;
            if (resolveOptions.ResolveOptions == null)
            {
                return query;
            }

            return query;
        }
    }
}
