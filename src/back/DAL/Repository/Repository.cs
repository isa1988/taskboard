﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
using Core.Helper;
using Core.Contract;
using Core.Entity;
using DAL.Data;
using Primitive.MyException;
using Core.Filters;
using Primitive;

namespace DAL.Repository
{
    public abstract class RepositoryBase<T, TOrder>
        where T : class
        where TOrder : Enum
    {
        protected virtual void ClearDbSetForInclude(List<T> entities)
        {
            foreach (var entity in entities)
            {
                ClearDbSetForInclude(entity);
            }
        }

        protected IQueryable<T> GetQueryTrackingBehavior(IQueryable<T> query, QueryOfTrackingBehaviorFilter querySetting)
        {
            if (querySetting == null)
            {
                return query;
            }
            else if (querySetting.TrackingBehavior == TrackingBehavior.AsNoTracking)
            {
                return query.AsNoTracking();
            }

            return query;
        }

        protected async Task<List<T>> GetListAsync(IQueryable<T> query, QueryOptions<T, TOrder> pagingOrderSetting)
        {
            List<T> entities = await GetListAsync<T>(query, pagingOrderSetting);
            ClearDbSetForInclude(entities);
            return entities;
        }

        protected async Task<List<T>> GetListAsync<TAny>(IQueryable<T> query, QueryOptions<TAny, TOrder> pagingOrderSetting)
        {
            query = GetQueryTrackingBehavior(query, pagingOrderSetting);
            if (pagingOrderSetting == null || pagingOrderSetting.PagingOrderSetting == null)
            {
                return await query.ToListAsync();
            }
            query = OrderSort(query, pagingOrderSetting);
            if (pagingOrderSetting != null && pagingOrderSetting.PagingOrderSetting.PageSetting != null)
            {
                query = query.Skip(pagingOrderSetting.PagingOrderSetting.PageSetting.StartPosition)
                    .Take(pagingOrderSetting.PagingOrderSetting.PageSetting.PageSize);
            }

            List<T> entities = await query.ToListAsync();
            ClearDbSetForInclude(entities);
            return entities;
        }

        protected abstract void ClearDbSetForInclude(T entity);

        protected abstract IQueryable<T> ResolveInclude(QueryOfTrackingBehaviorFilter resolveOptions);
        protected abstract IQueryable<T> OrderSort(IQueryable<T> query, IOrderSetting<TOrder> pagingOrderSetting);
    }


    public abstract class Repository<T, TOrder> : RepositoryBase<T, TOrder>, IRepository<T, TOrder> 
        where T : class, IEntity
        where TOrder: Enum
    {
        public Repository(TaskBoardContext contextDB)
        {
            this.contextDB = contextDB;
            this.dbSet = this.contextDB.Set<T>();
            this.dbSetQueryable = dbSet;
        }

        protected TaskBoardContext contextDB;
        protected DbSet<T> dbSet { get; set; }
        protected IQueryable<T> dbSetQueryable { get; set; }
        public virtual async Task<T> AddAsync(T entity)
        {
            var entry = await dbSet.AddAsync(entity);

            return entry.Entity;
        }
        public virtual async Task<List<T>> GetAllAsync(QueryOptions<T, TOrder> filter)
        {
            var query = ResolveInclude(filter);
            
            var entities = await GetListAsync(query, filter);

            return entities;
        }

        public virtual async Task<int> GetAllCountAsync(QueryOptionsCount<T> filter)
        {
            var query = ResolveInclude(filter);
            
            var count = await query.CountAsync();
            return count;
        }

        public virtual async Task<EntityEntry<T>> UpdateAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                return dbSet.Update(entity);
            });
            var entry = await task;
            return entry;
        }

        public virtual async Task<EntityEntry<T>> DeleteAsync(T entity)
        {
            Task<EntityEntry<T>> task = new Task<EntityEntry<T>>(() =>
            {
                return dbSet.Remove(entity);
            });
            var entry = await task;
            return entry;
        }

        public virtual EntityEntry<T> Update(T entity)
        {
            entity.ModifyDate = DateTime.UtcNow;
            return dbSet.Update(entity);
        }

        public virtual EntityEntry<T> Delete(T entity)
        {
            return dbSet.Remove(entity);
        }

        public virtual async Task SaveAsync()
        {
            await contextDB.SaveChangesAsync();
        }
    }

    public abstract class Repository<T, TId, TOrder> : Repository<T, TOrder>, IRepository<T, TId, TOrder>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
        where TOrder: Enum
    {
        public Repository(TaskBoardContext contextDB)
            : base(contextDB)
        {
        }

        public virtual async Task<T> GetByIdAsync(QueryOptionsOne<TId> filter)
        {
            var query = ResolveInclude(filter);
            query = GetQueryTrackingBehavior(query, filter);
            var entity = await query.FirstOrDefaultAsync(x => x.Id.Equals(filter.Filter));
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }
    }

    public abstract class RepositoryGuid<T, TOrder> : Repository<T, Guid, TOrder>, IRepository<T, Guid, TOrder>
        where T : class, IEntity<Guid>
        where TOrder : Enum
    {
        public RepositoryGuid(TaskBoardContext contextDB)
            : base(contextDB)
        {
        }

        public override async Task<T> GetByIdAsync(QueryOptionsOne<Guid> id)
        {
            var query = ResolveInclude(id);
            query = GetQueryTrackingBehavior(query, id);
            var entity = await query.FirstOrDefaultAsync(x => x.Id == id.Filter);
            if (entity == null)
                throw new RawDbNullException("Не найдено значение по идентификатору");
            ClearDbSetForInclude(entity);
            return entity;
        }
    }
}
