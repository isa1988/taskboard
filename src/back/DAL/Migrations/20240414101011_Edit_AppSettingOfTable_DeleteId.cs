﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DAL.Migrations
{
    /// <inheritdoc />
    public partial class Edit_AppSettingOfTable_DeleteId : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AppSettingOfTable",
                table: "AppSettingOfTable");

            migrationBuilder.DropIndex(
                name: "IX_AppSettingOfTable_AppSettingId",
                table: "AppSettingOfTable");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "AppSettingOfTable");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AppSettingOfTable",
                table: "AppSettingOfTable",
                columns: new[] { "AppSettingId", "Type" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AppSettingOfTable",
                table: "AppSettingOfTable");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "AppSettingOfTable",
                type: "uniqueidentifier",
                nullable: false,
                defaultValueSql: "newid()");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AppSettingOfTable",
                table: "AppSettingOfTable",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_AppSettingOfTable_AppSettingId",
                table: "AppSettingOfTable",
                column: "AppSettingId");
        }
    }
}
