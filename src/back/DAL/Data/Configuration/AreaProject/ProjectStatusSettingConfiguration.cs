﻿using Core.Entity.AreaProject;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.AreaProject
{
    internal class ProjectStatusSettingConfiguration : IEntityTypeConfiguration<ProjectStatusSetting>
    {
        public void Configure(EntityTypeBuilder<ProjectStatusSetting> builder)
        {
            builder.HasKey(e => new { e.ProjectStatusId, e.Status });
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.HasOne(e => e.ProjectStatus)
                .WithMany(p => p.ProjectStatusSettingList)
                .HasForeignKey(d => d.ProjectStatusId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
