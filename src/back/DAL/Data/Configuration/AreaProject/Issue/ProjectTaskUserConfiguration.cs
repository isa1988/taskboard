﻿using Core.Entity.AreaProject.Issue;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.AreaProject.Issue
{
    internal class ProjectTaskUserConfiguration : IEntityTypeConfiguration<ProjectTaskUser>
    {
        public void Configure(EntityTypeBuilder<ProjectTaskUser> builder)
        {
            builder.HasKey(e => new { e.ProjectTaskId, e.UserId });
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.HasOne(e => e.ProjectTask)
                .WithMany(p => p.ProjectTaskUserList)
                .HasForeignKey(d => d.ProjectTaskId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.User)
                .WithMany(p => p.ProjectTaskUserList)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
