﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Core.Entity.AreaProject.Issue;

namespace DAL.Data.Configuration.AreaProject.Issue
{
    internal class ProjectTaskCommentConfiguration : IEntityTypeConfiguration<ProjectTaskComment>
    {
        public void Configure(EntityTypeBuilder<ProjectTaskComment> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.Name).IsRequired().HasColumnType("nvarchar(4000)");

            builder.HasOne(e => e.User)
                .WithMany(p => p.ProjectTaskCommentList)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.ProjectTask)
                .WithMany(p => p.ProjectTaskCommentList)
                .HasForeignKey(d => d.ProjectTaskId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
