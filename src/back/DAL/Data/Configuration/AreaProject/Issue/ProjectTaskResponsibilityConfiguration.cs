﻿using Core.Entity.AreaProject.Issue;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.AreaProject.Issue
{
    internal class ProjectTaskResponsibilityConfiguration : IEntityTypeConfiguration<ProjectTaskResponsibility>
    {
        public void Configure(EntityTypeBuilder<ProjectTaskResponsibility> builder)
        {
            builder.HasKey(e => new { e.ProjectTaskId, e.ProjectResponsibilityId });
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.HasOne(e => e.ProjectTask)
                .WithMany(p => p.ProjectTaskResponsibilityList)
                .HasForeignKey(d => d.ProjectTaskId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.ProjectResponsibility)
                .WithMany(p => p.ProjectTaskResponsibilityList)
                .HasForeignKey(d => d.ProjectResponsibilityId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
