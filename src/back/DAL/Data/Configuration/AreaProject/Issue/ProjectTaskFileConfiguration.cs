﻿using Core.Entity.AreaProject.Issue;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.AreaProject.Issue
{
    public class ProjectTaskFileConfiguration : IEntityTypeConfiguration<ProjectTaskFile>
    {
        public void Configure(EntityTypeBuilder<ProjectTaskFile> builder)
        {
            builder.HasKey(e => new { e.ProjectTaskId, e.FileAreaId });
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            
            builder.HasOne(e => e.ProjectTask)
                .WithMany(p => p.ProjectTaskFileList)
                .HasForeignKey(d => d.ProjectTaskId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.FileArea)
                .WithMany(p => p.ProjectTaskFileList)
                .HasForeignKey(d => d.FileAreaId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
