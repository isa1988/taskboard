﻿
using Core.Entity.AreaProject.Issue;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.AreaProject.Issue
{
    internal class ProjectTaskConfiguration : IEntityTypeConfiguration<ProjectTask>
    {
        public void Configure(EntityTypeBuilder<ProjectTask> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.Name).IsRequired().HasMaxLength(500);
            builder.Property(e => e.Description).IsRequired().HasColumnType("nvarchar(4000)");

            builder.HasOne(e => e.Project)
                .WithMany(p => p.ProjectTaskList)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.ProjectStatus)
                .WithMany(p => p.ProjectTaskList)
                .HasForeignKey(d => d.ProjectStatusId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.ProjectType)
                .WithMany(p => p.ProjectTaskList)
                .HasForeignKey(d => d.ProjectTypeId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
