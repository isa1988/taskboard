﻿using Core.Entity.AreaProject.Issue;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.AreaProject.Issue
{
    public class ProjectTaskCommentFileConfiguration : IEntityTypeConfiguration<ProjectTaskCommentFile>
    {
        public void Configure(EntityTypeBuilder<ProjectTaskCommentFile> builder)
        {
            builder.HasKey(e => new { e.ProjectTaskCommentId, e.FileAreaId });
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            
            builder.HasOne(e => e.ProjectTaskComment)
                .WithMany(p => p.ProjectTaskCommentFileList)
                .HasForeignKey(d => d.ProjectTaskCommentId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(e => e.FileArea)
                .WithMany(p => p.ProjectTaskCommentFileList)
                .HasForeignKey(d => d.FileAreaId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
