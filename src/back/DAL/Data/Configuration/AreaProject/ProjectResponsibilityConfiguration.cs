﻿using Core.Entity.AreaProject;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.AreaProject
{
    internal class ProjectResponsibilityConfiguration : IEntityTypeConfiguration<ProjectResponsibility>
    {
        public void Configure(EntityTypeBuilder<ProjectResponsibility> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.Name).IsRequired().HasMaxLength(256);

            builder.HasOne(e => e.Project)
                .WithMany(p => p.ProjectResponsibilityList)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
