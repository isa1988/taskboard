﻿using Core.Entity.Setting;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.Settings
{
    internal class FileAreaConfiguration : IEntityTypeConfiguration<FileArea>
    {
        public void Configure(EntityTypeBuilder<FileArea> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");

            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.Property(e => e.Name).IsRequired().HasMaxLength(500);
            builder.Property(e => e.Path).IsRequired().HasMaxLength(700);
            builder.Property(e => e.Type).IsRequired().HasMaxLength(50);
            builder.Property(e => e.Desc).IsRequired().HasColumnType("nvarchar(2000)");
        }
    }
}
