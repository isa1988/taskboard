﻿using Core.Entity.Setting;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.Settings
{
    internal class AppSettingConfiguration : IEntityTypeConfiguration<AppSetting>
    {
        public void Configure(EntityTypeBuilder<AppSetting> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");

            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.Property(e => e.Name).IsRequired().HasMaxLength(100);
        }
    }
}
