﻿using Core.Entity.Setting;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data.Configuration.Settings
{
    internal class AppSettingOfTableConfiguration : IEntityTypeConfiguration<AppSettingOfTable>
    {
        public void Configure(EntityTypeBuilder<AppSettingOfTable> builder)
        {
            builder.HasKey(e => new { e.AppSettingId, e.Type });

            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.Property(e => e.Value).HasColumnType("nvarchar(2000)");
            builder.Property(e => e.ValueJson).HasColumnType("nvarchar(4000)");

            builder.HasOne(e => e.AppSetting)
                .WithMany(p => p.AppSettingOfTableList)
                .HasForeignKey(d => d.AppSettingId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
