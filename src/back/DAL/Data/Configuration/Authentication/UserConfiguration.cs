﻿using Core.Entity.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Data.Configuration.Authentication
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");

            builder.Property(e => e.FirstName).IsRequired().HasMaxLength(256);
            builder.Property(e => e.MiddleName).HasMaxLength(256);
            builder.Property(e => e.LastName).IsRequired().HasMaxLength(256);

            builder.Property(e => e.Email).IsRequired().HasMaxLength(256);
            builder.Property(e => e.Password).IsRequired().HasColumnType("nvarchar(2000)");

            builder.Property(e => e.Login).IsRequired().HasMaxLength(256);
        }
    }
}
