﻿using Core.Entity.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Data.Configuration.Authentication
{
    class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(p => p.Id).HasDefaultValueSql("newid()");
            builder.Property(e => e.CreateDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.ModifyDate).IsRequired().HasColumnType("datetime")
                .HasDefaultValueSql("getutcdate()");
            builder.Property(e => e.Name).IsRequired().HasMaxLength(256);
            builder.HasIndex(e => e.Name).IsUnique();
        }
    }
}
