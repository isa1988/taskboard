﻿using Core.Entity.AreaProject;
using Core.Entity.AreaProject.Issue;
using Core.Entity.Authentication;
using Core.Entity.Setting;
using DAL.Data.Configuration.AreaProject;
using DAL.Data.Configuration.AreaProject.Issue;
using DAL.Data.Configuration.Authentication;
using DAL.Data.Configuration.Settings;
using Microsoft.EntityFrameworkCore;

namespace DAL.Data
{
    public class TaskBoardContext : DbContext
    {
        public TaskBoardContext(DbContextOptions<TaskBoardContext> options)
            : base(options)
        {
        }
        public virtual DbSet<UserRole> UserRole { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<User> User { get; set; }

        public virtual DbSet<AppSetting> AppSetting { get; set; }
        public virtual DbSet<AppSettingOfTable> AppSettingOfTable { get; set; }
        public virtual DbSet<FileArea> FileArea { get; set; }

        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<ProjectResponsibility> ProjectResponsibility { get; set; }
        public virtual DbSet<ProjectStatus> ProjectStatus { get; set; }
        public virtual DbSet<ProjectStatusSetting> ProjectStatusSetting { get; set; }
        public virtual DbSet<ProjectType> ProjectType { get; set; }
        public virtual DbSet<ProjectUser> ProjectUser { get; set; }
        public virtual DbSet<ProjectTask> ProjectTask { get; set; }
        public virtual DbSet<ProjectTaskComment> ProjectTaskComment { get; set; }
        public virtual DbSet<ProjectTaskCommentFile> ProjectTaskCommentFile { get; set; }
        public virtual DbSet<ProjectTaskFile> ProjectTaskFile { get; set; }
        public virtual DbSet<ProjectTaskResponsibility> ProjectTaskResponsibility { get; set; }
        public virtual DbSet<ProjectTaskUser> ProjectTaskUser { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
            modelBuilder.ApplyConfiguration(new SessionConfiguration());

            modelBuilder.ApplyConfiguration(new AppSettingConfiguration());
            modelBuilder.ApplyConfiguration(new AppSettingOfTableConfiguration());
            modelBuilder.ApplyConfiguration(new FileAreaConfiguration());

            modelBuilder.ApplyConfiguration(new ProjectConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectResponsibilityConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectStatusConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectStatusSettingConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectUserConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTaskConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTaskCommentConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTaskCommentFileConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTaskFileConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTaskResponsibilityConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTaskUserConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
