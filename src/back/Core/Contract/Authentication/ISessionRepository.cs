﻿using Core.Entity.Authentication;
using Core.Filters;
using Core.Helper;
using Primitive.PagingOrderSettings.Authentication;

namespace Core.Contract.Authentication
{
    public interface ISessionRepository : IRepository<Session, Guid, OrderSessionFields>
    {
        Task<Session> GetActiveByTokenAsync(QueryOptionsOne<string> token);
        Task<Guid> GetUserIdByActiveSession(QueryOptionsOne<Guid> sessionId);

        Task CloseAllOpenSessionAsync(QueryOptionsOne<Guid> userId);

        Task<List<Session>> GetByUserListAsync(QueryOptions<List<Guid>, OrderSessionFields> userListId);

        Task<List<Session>> GetAllByUserRoleAsync(QueryOptions<Guid, OrderSessionFields> userRoleId);
    }
}
