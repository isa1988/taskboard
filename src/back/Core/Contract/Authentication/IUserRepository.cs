﻿using Core.Entity.Authentication;
using Core.Filters;
using Core.Filters.Authentication;
using Core.Filters.Authentication.Check;
using Core.Helper;
using Primitive.PagingOrderSettings.Authentication;

namespace Core.Contract.Authentication
{
    public interface IUserRepository : IRepositoryForDeleted<User, Guid, OrderUserFields>
    {
        Task<List<User>> GetAllFromSearchAsync(QueryOptionsForDelete<string, OrderUserFields> filter);
        Task<int> GetAllFromSearchCountAsync(QueryOptionsForDeleteCount<string> filter);
        Task<bool> IsEqualEmailAsync(QueryOptionsForDeleteOne<CheckEmailOfUserFilter> filter);
        Task<User?> GetUserAsync(QueryOptionsForDeleteOne<LoginUserFilter> filter);
        string GetEncryptedPassword(QueryOptionsForDeleteOne<string> password);
    }
}
