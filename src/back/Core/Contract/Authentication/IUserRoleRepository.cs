﻿using Core.Entity.Authentication;
using Core.Filters;
using Core.Filters.Authentication;
using Core.Helper;
using Primitive.PagingOrderSettings.Authentication;

namespace Core.Contract.Authentication
{
    public interface IUserRoleRepository : IRepository<UserRole, OrderUserRoleFields>
    {
        Task<UserRole?> GetByRoleForUserAsync(QueryOptionsOne<UserRoleFilter> filter);
        Task<List<UserRole>> GetByUserListAsync(QueryOptions<Guid, OrderUserRoleFields> userId);
        Task<int> GetByUserCountAsync(QueryOptionsCount<Guid> userId);
    }
}
