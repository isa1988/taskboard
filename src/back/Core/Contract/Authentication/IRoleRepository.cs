﻿using Core.Entity.Authentication;
using Core.Filters;
using Core.Filters.Authentication.Check;
using Primitive.PagingOrderSettings.Authentication;

namespace Core.Contract.Authentication
{
    public interface IRoleRepository : IRepositoryForDeleted<Role, Guid, OrderRoleFields>
    {
        Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfRoleFilter> filter);
    }
}
