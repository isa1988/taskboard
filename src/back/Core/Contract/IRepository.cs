﻿using Core.Entity;
using Core.Filters;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Primitive;

namespace Core.Contract
{
    public interface IRepository<T, TOrder>
        where T : class, IEntity
        where TOrder : Enum
    {
        Task<T> AddAsync(T entity);
        Task<List<T>> GetAllAsync(QueryOptions<T, TOrder> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<T> filter);
        EntityEntry<T> Update(T entity);
        EntityEntry<T> Delete(T entity);
        Task SaveAsync();
    }
    public interface IRepository<T, TId, TOrder> : IRepository<T, TOrder>
        where T : class, IEntity<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        Task<T> GetByIdAsync(QueryOptionsOne<TId> filter);
    }
}
