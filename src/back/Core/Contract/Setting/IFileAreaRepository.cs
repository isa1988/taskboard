﻿using Core.Entity.Setting;
using Primitive.PagingOrderSettings.Setting;

namespace Core.Contract.Setting
{
    public interface IFileAreaRepository : IRepositoryForDeleted<FileArea, Guid, OrderFileAreaFields>
    {
    }
}
