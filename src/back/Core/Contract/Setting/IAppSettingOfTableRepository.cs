﻿using Core.Entity.Setting;
using Core.Filters.Setting;
using Core.Filters;
using Primitive.PagingOrderSettings.Setting;

namespace Core.Contract.Setting
{
    public interface IAppSettingOfTableRepository : IRepository<AppSettingOfTable, OrderAppSettingOfTableFields>
    {
        Task<List<AppSettingOfTable>> GetAllAsync(QueryOptions<AppSettingOfTableFilter, OrderAppSettingOfTableFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<AppSettingOfTableFilter> filter);
        Task<AppSettingOfTable?> GetByIdAsync(QueryOptionsForDeleteOne<AppSettingOfTableOneFilter> filter);
    }
}
