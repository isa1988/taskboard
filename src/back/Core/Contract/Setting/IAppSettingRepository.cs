﻿using Core.Entity.Setting;
using Core.Filters;
using Core.Filters.Setting;
using Core.Filters.Setting.Check;
using Primitive.PagingOrderSettings.Setting;

namespace Core.Contract.Setting
{
    public interface IAppSettingRepository : IRepository<AppSetting, Guid, OrderAppSettingFields>
    {
        Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfAppSettingFilter> filter);
        Task<AppSetting?> GetByIdAsync(QueryOptionsForDeleteOne<AppSettingOneFilter> filter);
    }
}
