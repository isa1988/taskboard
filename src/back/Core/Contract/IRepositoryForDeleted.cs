﻿using Core.Entity;
using Core.Filters;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Core.Contract
{
    public interface IRepositoryForDeleted<T, TOrder> : IRepository<T, TOrder>
        where T : class, IEntityForDelete
        where TOrder : Enum
    {
        Task<List<T>> GetAllAsync(QueryOptionsForDelete<T, TOrder> filter);
        Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<T> filter);
        EntityEntry<T> DeleteFromDbAsync(T entity);
    }
    public interface IRepositoryForDeleted<T, TId, TOrder> : IRepositoryForDeleted<T, TOrder>
        where T : class, IEntityForDelete<TId>
        where TId : IEquatable<TId>
        where TOrder : Enum
    {
        Task<T> GetByIdAsync(QueryOptionsOne<TId> filter);

        Task<T> GetByIdAsync(QueryOptionsForDeleteOne<TId> filter);
    }
}
