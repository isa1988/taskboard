﻿using Core.Entity.AreaProject.Issue;
using Core.Filters.Project.Issue;
using Core.Filters;
using Primitive.PagingOrderSettings.Project.Issue;

namespace Core.Contract.Project.Issue
{
    public interface IProjectTaskCommentRepository : IRepository<ProjectTaskComment, Guid, OrderProjectTaskCommentFields>
    {
        Task<List<ProjectTaskComment>> GetAllAsync(QueryOptions<ProjectTaskCommentFilter, OrderProjectTaskCommentFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskCommentFilter> filter);
    }
}
