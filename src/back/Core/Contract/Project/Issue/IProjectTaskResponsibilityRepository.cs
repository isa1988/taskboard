﻿using Core.Entity.AreaProject.Issue;
using Core.Filters.Project.Issue;
using Core.Filters;
using Primitive.PagingOrderSettings.Project.Issue;

namespace Core.Contract.Project.Issue
{
    public interface IProjectTaskResponsibilityRepository : IRepository<ProjectTaskResponsibility, OrderProjectTaskResponsibilityFields>
    {
        Task<List<ProjectTaskResponsibility>> GetAllAsync(QueryOptions<ProjectTaskResponsibilityFilter, OrderProjectTaskResponsibilityFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskResponsibilityFilter> filter);
        Task<ProjectTaskResponsibility?> GetByIdAsync(QueryOptionsOne<ProjectTaskResponsibilityOneFilter> filter);
    }
}
