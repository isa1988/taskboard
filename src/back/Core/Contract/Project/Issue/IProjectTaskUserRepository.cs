﻿using Core.Entity.AreaProject.Issue;
using Core.Filters.Project.Issue;
using Core.Filters;
using Primitive.PagingOrderSettings.Project.Issue;

namespace Core.Contract.Project.Issue
{
    public interface IProjectTaskUserRepository : IRepository<ProjectTaskUser, OrderProjectTaskUserFields>
    {
        Task<List<ProjectTaskUser>> GetAllAsync(QueryOptions<ProjectTaskUserFilter, OrderProjectTaskUserFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskUserFilter> filter);
        Task<ProjectTaskUser?> GetByIdAsync(QueryOptionsOne<ProjectTaskUserOneFilter> filter);
    }
}
