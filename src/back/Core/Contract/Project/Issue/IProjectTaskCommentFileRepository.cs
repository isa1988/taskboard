﻿using Core.Entity.AreaProject.Issue;
using Core.Filters.Project.Issue;
using Core.Filters;
using Primitive.PagingOrderSettings.Project.Issue;

namespace Core.Contract.Project.Issue
{
    public interface IProjectTaskCommentFileRepository : IRepository<ProjectTaskCommentFile, OrderProjectTaskCommentFileFields>
    {
        Task<List<ProjectTaskCommentFile>> GetAllAsync(QueryOptions<ProjectTaskCommentFileFilter, OrderProjectTaskCommentFileFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskCommentFileFilter> filter);
        Task<ProjectTaskCommentFile?> GetByIdAsync(QueryOptionsOne<ProjectTaskCommentFileOneFilter> filter);
    }
}
