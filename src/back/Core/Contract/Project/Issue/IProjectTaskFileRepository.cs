﻿using Core.Entity.AreaProject.Issue;
using Core.Filters.Project.Issue;
using Core.Filters;
using Primitive.PagingOrderSettings.Project.Issue;

namespace Core.Contract.Project.Issue
{
    public interface IProjectTaskFileRepository : IRepository<ProjectTaskFile, OrderProjectTaskFileFields>
    {
        Task<List<ProjectTaskFile>> GetAllAsync(QueryOptions<ProjectTaskFileFilter, OrderProjectTaskFileFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<ProjectTaskFileFilter> filter);
        Task<ProjectTaskFile?> GetByIdAsync(QueryOptionsOne<ProjectTaskFileOneFilter> filter);
    }
}
