﻿using Core.Entity.AreaProject.Issue;
using Core.Filters;
using Primitive.PagingOrderSettings.Project.Issue;
using Core.Filters.Project.Issue;

namespace Core.Contract.Project.Issue
{
    public interface IProjectTaskRepository : IRepositoryForDeleted<ProjectTask, Guid, OrderProjectTaskFields>
    {
        Task<List<ProjectTask>> GetAllAsync(QueryOptionsForDelete<ProjectTaskFilter, OrderProjectTaskFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectTaskFilter> filter);
    }
}
