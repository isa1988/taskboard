﻿using Core.Filters;
using Core.Filters.Project;
using Core.Filters.Project.Check;
using Primitive.PagingOrderSettings.Project;

namespace Core.Contract.Project
{
    public interface IProjectRepository : IRepositoryForDeleted<Core.Entity.AreaProject.Project, Guid, OrderProjectFields>
    {
        Task<List<Core.Entity.AreaProject.Project>> GetAllAsync(QueryOptionsForDelete<ProjectFilter, OrderProjectFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectFilter> filter);
        Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfProjectFilter> filter);
    }
}
