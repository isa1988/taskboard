﻿using Core.Entity.AreaProject;
using Core.Filters;
using Core.Filters.Project;
using Core.Filters.Project.Check;
using Primitive.PagingOrderSettings.Project;

namespace Core.Contract.Project
{
    public interface IProjectStatusRepository : IRepositoryForDeleted<ProjectStatus, Guid, OrderProjectStatusFields>
    {
        Task<List<ProjectStatus>> GetAllAsync(QueryOptionsForDelete<ProjectStatusFilter, OrderProjectStatusFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectStatusFilter> filter);
        Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfProjectStatusFilter> filter);
    }
}
