﻿using Core.Entity.AreaProject;
using Core.Filters;
using Core.Filters.Project;
using Primitive.PagingOrderSettings.Project;

namespace Core.Contract.Project
{
    public interface IProjectStatusSettingRepository : IRepository<ProjectStatusSetting, OrderProjectStatusSettingFields>
    {
        Task<List<ProjectStatusSetting>> GetAllAsync(QueryOptions<ProjectStatusSettingFilter, OrderProjectStatusSettingFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<ProjectStatusSettingFilter> filter);
        Task<ProjectStatusSetting?> GetByIdAsync(QueryOptionsOne<ProjectStatusSettingOneFilter> filter);
    }
}
