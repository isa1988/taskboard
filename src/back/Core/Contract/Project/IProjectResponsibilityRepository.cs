﻿using Core.Entity.AreaProject;
using Core.Filters;
using Core.Filters.Project;
using Core.Filters.Project.Check;
using Primitive.PagingOrderSettings.Project;

namespace Core.Contract.Project
{
    public interface IProjectResponsibilityRepository : IRepositoryForDeleted<ProjectResponsibility, Guid, OrderProjectResponsibilityFields>
    {
        Task<List<ProjectResponsibility>> GetAllAsync(QueryOptionsForDelete<ProjectResponsibilityFilter, OrderProjectResponsibilityFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectResponsibilityFilter> filter);
        Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfProjectResponsibilityFilter> filter);
    }
}
