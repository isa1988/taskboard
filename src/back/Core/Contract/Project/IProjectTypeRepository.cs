﻿using Core.Entity.AreaProject;
using Core.Filters;
using Core.Filters.Project;
using Core.Filters.Project.Check;
using Primitive.PagingOrderSettings.Project;

namespace Core.Contract.Project
{
    public interface IProjectTypeRepository : IRepositoryForDeleted<ProjectType, OrderProjectTypeFields>
    {
        Task<List<ProjectType>> GetAllAsync(QueryOptionsForDelete<ProjectTypeFilter, OrderProjectTypeFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsForDeleteCount<ProjectTypeFilter> filter);
        Task<bool> IsEqualsNameAsync(QueryOptionsForDeleteOne<CheckNameOfProjectTypeFilter> filter);
    }
}
