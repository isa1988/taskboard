﻿using Core.Entity.AreaProject;
using Core.Filters;
using Core.Filters.Project;
using Primitive.PagingOrderSettings.Project;

namespace Core.Contract.Project
{
    public interface IProjectUserRepository : IRepository<ProjectUser, OrderProjectUserFields>
    {
        Task<List<ProjectUser>> GetAllAsync(QueryOptions<ProjectUserFilter, OrderProjectUserFields> filter);
        Task<int> GetAllCountAsync(QueryOptionsCount<ProjectUserFilter> filter);
        Task<bool> GetByIdAsync(QueryOptionsOne<ProjectUserOneFilter> filter);
    }
}
