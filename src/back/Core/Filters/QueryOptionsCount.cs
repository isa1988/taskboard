﻿namespace Core.Filters
{
    public record QueryOptionsCount<T> : QueryOfTrackingBehaviorFilter
    {
        public T Filter { get; set; }
    }
}
