﻿using Primitive.PagingOrderSettings;

namespace Core.Filters
{
    public interface IQuerySettingSelectForDelete
    {
        public SettingSelectForDelete SettingSelectForDelete { get; set; }
    }
}
