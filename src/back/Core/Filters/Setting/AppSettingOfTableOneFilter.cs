﻿using Primitive.AppSetting;

namespace Core.Filters.Setting
{
    public record AppSettingOfTableOneFilter
    {
        public Guid AppSettingId { get; set; }
        public AppSettingOfTableType Type { get; set; }
    }
}
