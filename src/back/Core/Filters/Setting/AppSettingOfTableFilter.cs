﻿namespace Core.Filters.Setting
{
    public record AppSettingOfTableFilter
    {
        public Guid AppSettingId { get; set; }
    }
}
