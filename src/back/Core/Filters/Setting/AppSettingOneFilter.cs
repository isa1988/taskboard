﻿namespace Core.Filters.Setting
{
    public record AppSettingOneFilter
    {
        public string Name { get; set; } = string.Empty;
    }
}
