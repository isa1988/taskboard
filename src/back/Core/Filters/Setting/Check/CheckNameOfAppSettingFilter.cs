﻿namespace Core.Filters.Setting.Check
{
    public record CheckNameOfAppSettingFilter : CheckFilterById
    {
        public string Name { get; set; } = string.Empty;
    }
}
