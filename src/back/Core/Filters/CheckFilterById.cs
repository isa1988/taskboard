﻿namespace Core.Filters
{
    public record CheckFilterById
    {
        public Guid? Id { get; set; }
    }
}
