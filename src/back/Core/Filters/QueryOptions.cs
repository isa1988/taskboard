﻿using Core.Helper;

namespace Core.Filters
{
    public record QueryOptions<T, TOrder> : QueryOfTrackingBehaviorFilter, IOrderSetting<TOrder>
        where TOrder : Enum
    {
        public T Filter { get; set; }
        public PagingOrderSetting<TOrder>? PagingOrderSetting { get; set; }
    }
}
