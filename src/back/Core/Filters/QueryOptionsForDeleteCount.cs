﻿using Primitive.PagingOrderSettings;

namespace Core.Filters
{
    public record QueryOptionsForDeleteCount<T> : QueryOptionsCount<T>, IQuerySettingSelectForDelete
    {
        public SettingSelectForDelete SettingSelectForDelete { get; set; }
    }
}
