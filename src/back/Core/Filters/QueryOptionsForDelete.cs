﻿using Primitive.PagingOrderSettings;

namespace Core.Filters
{
    public record QueryOptionsForDelete<T, TOrder> : QueryOptions<T, TOrder>, IQuerySettingSelectForDelete
        where TOrder : Enum
    {
        public SettingSelectForDelete SettingSelectForDelete { get; set; }
    }
}
