﻿namespace Core.Filters.Authentication.Check
{
    public record CheckEmailOfUserFilter : CheckFilterById
    {
        public string Email { get; set; } = string.Empty;
    }
}
