﻿namespace Core.Filters.Authentication.Check
{
    public record CheckNameOfRoleFilter : CheckFilterById
    {
        public string Name { get; set; } = string.Empty;
    }
}
