﻿namespace Core.Filters.Authentication
{
    public record UserRoleFilter
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
    }
}
