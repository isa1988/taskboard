﻿using Primitive.Project;

namespace Core.Filters.Project
{
    public record ProjectStatusSettingOneFilter
    {
        public Guid ProjectStatusId { get; set; }
        public ProjectStatusSettingEnum Status { get; set; }
    }
}
