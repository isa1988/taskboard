﻿namespace Core.Filters.Project
{
    public record ProjectResponsibilityFilter
    {
        public string? Name { get; set; }
        public Guid ProjectId { get; set; }
    }
}
