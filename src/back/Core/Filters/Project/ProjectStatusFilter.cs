﻿namespace Core.Filters.Project
{
    public record ProjectStatusFilter
    {
        public string? Name { get; set; }
        public Guid ProjectId { get; set; }
    }
}
