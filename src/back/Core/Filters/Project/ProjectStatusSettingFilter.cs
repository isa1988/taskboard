﻿namespace Core.Filters.Project
{
    public record ProjectStatusSettingFilter
    {
        public Guid ProjectStatusId { get; set; }
    }
}
