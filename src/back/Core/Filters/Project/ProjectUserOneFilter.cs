﻿
namespace Core.Filters.Project
{
    public record ProjectUserOneFilter
    {
        public Guid ProjectId { get; set; }
        public Guid UserId { get; set; }
    }
}
