﻿namespace Core.Filters.Project.Check
{
    public record CheckNameOfProjectStatusFilter : CheckFilterByProjectIdFilter
    {
        public string Name { get; set; } = string.Empty;
    }
}
