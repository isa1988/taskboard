﻿namespace Core.Filters.Project.Check
{
    public record CheckNameOfProjectFilter : CheckFilterById
    {
        public string Name { get; set; } = string.Empty;
    }
}
