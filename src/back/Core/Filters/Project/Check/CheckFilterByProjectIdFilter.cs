﻿namespace Core.Filters.Project.Check
{
    public record CheckFilterByProjectIdFilter : CheckFilterById
    {
        public Guid ProjectId { get; set; }
    }
}
