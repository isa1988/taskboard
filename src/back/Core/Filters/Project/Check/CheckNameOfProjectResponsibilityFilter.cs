﻿namespace Core.Filters.Project.Check
{
    public record CheckNameOfProjectResponsibilityFilter : CheckFilterByProjectIdFilter
    {
        public string Name { get; set; } = string.Empty;
    }
}
