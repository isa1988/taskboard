﻿namespace Core.Filters.Project.Check
{
    public record CheckNameOfProjectTypeFilter : CheckFilterByProjectIdFilter
    {
        public string Name { get; set; } = string.Empty;
    }
}
