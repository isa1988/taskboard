﻿namespace Core.Filters.Project
{
    public record ProjectTypeFilter
    {
        public string? Name { get; set; }
        public Guid ProjectId { get; set; }
    }
}
