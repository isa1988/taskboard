﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskResponsibilityFilter
    {
        public Guid ProjectTaskId { get; set; }
    }
}
