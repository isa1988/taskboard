﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskFileFilter
    {
        public Guid ProjectTaskId { get; set; }
    }
}
