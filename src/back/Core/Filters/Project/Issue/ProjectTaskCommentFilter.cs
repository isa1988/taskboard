﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskCommentFilter
    {
        public Guid ProjectTaskId { get; set; }
    }
}
