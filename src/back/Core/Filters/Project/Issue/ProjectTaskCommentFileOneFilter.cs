﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskCommentFileOneFilter
    {
        public Guid ProjectTaskCommentId { get; set; }
        public Guid FileAreaId { get; set; }
    }
}
