﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskCommentFileFilter
    {
        public Guid ProjectTaskCommentId { get; set; }
    }
}
