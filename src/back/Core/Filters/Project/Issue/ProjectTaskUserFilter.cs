﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskUserFilter
    {
        public Guid ProjectTaskId { get; set; }
    }
}
