﻿using Primitive.Project;

namespace Core.Filters.Project.Issue
{
    public record ProjectTaskUserForTaskFilter
    {
        public Guid? UserId { get; set; }
        public ProjectTaskUserType? Type { get; set; }
    }
}
