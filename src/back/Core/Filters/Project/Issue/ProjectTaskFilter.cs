﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskFilter
    {
        public string? Name { get; set; }
        public long? Number { get; set; }
        public Guid ProjectId { get; set; }
        public Guid? ProjectStatusId { get; set; }
        public List<ProjectTaskUserForTaskFilter> ProjectTaskUserList { get; set; } = new List<ProjectTaskUserForTaskFilter>();
}
}
