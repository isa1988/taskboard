﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskFileOneFilter
    {
        public Guid ProjectTaskId { get; set; }
        public Guid FileAreaId { get; set; }
    }
}
