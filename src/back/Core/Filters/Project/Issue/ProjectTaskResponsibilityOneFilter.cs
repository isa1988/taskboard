﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskResponsibilityOneFilter
    {
        public Guid ProjectResponsibilityId { get; set; }
        public Guid ProjectTaskId { get; set; }
    }
}
