﻿namespace Core.Filters.Project.Issue
{
    public record ProjectTaskUserOneFilter
    {
        public Guid ProjectTaskId { get; set; }
        public Guid UserId { get; set; }
    }
}
