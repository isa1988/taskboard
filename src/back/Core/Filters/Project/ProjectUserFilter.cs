﻿namespace Core.Filters.Project
{
    public record ProjectUserFilter
    {
        public Guid ProjectId { get; set; }
    }
}
