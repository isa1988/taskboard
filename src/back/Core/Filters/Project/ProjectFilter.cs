﻿namespace Core.Filters.Project
{
    public record ProjectFilter
    {
        public string? Name { get; set; }
        public Guid? UserId { get; set; }
    }
}
