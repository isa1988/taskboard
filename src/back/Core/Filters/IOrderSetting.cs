﻿using Core.Helper;

namespace Core.Filters
{
    public interface IOrderSetting<TOrder>
        where TOrder : Enum
    {
        public PagingOrderSetting<TOrder>? PagingOrderSetting { get; set; }
    }
}
