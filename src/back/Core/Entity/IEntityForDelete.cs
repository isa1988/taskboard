﻿using Primitive;

namespace Core.Entity
{
    public interface IEntityForDelete : IEntity
    {
        Deleted IsDeleted { get; set; }
    }

    public interface IEntityForDelete<TId> : IEntityForDelete
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
