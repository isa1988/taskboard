﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Entity
{
    public interface IEntity
    {
        DateTime CreateDate { get; set; }
        DateTime ModifyDate { get; set; }
    }

    public interface IEntity<TId> : IEntity
        where TId : IEquatable<TId>
    {
        TId Id { get; set; }
    }
}
