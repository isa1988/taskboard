﻿namespace Core.Entity.AreaPermission
{
    public class Permission : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public List<PermissionController> PermissionControllerList { get; set; } = new List<PermissionController>();
        public List<PermissionRole> PermissionRoleList { get; set; } = new List<PermissionRole>();

    }
}
