﻿namespace Core.Entity.AreaPermission
{
    public class PermissionAction : IEntity<Guid>
    {
        public Guid Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public string Title { get; set; } = string.Empty;
        public string Action { get; set; } = string.Empty;

        public PermissionController? PermissionController { get; set; }
        public Guid PermissionControllerId { get; set; }
    }
}
