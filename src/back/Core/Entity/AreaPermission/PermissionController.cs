﻿namespace Core.Entity.AreaPermission
{
    public class PermissionController : IEntity<Guid>
    {
        public Guid Id { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public string Name { get; set; } = string.Empty;
        public Permission? Permission { get; set; }
        public Guid PermissionId { get; set; }

        public List<PermissionAction> PermissionActionList { get; set; } = new List<PermissionAction>();
    }
}
