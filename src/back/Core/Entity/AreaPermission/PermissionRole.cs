﻿using Core.Entity.Authentication;
using Primitive;

namespace Core.Entity.AreaPermission
{
    public class PermissionRole : IEntity
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }

        public Guid RoleId { get; set; }
        public Role? Role { get; set; }

        public Permission? Permission { get; set; }
        public Guid PermissionId { get; set; }

        public PermissionRoleStatus Status { get; set; }

    }
}
