﻿
using Primitive.AppSetting;

namespace Core.Entity.Setting
{
    public class AppSettingOfTable : IEntity
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid AppSettingId { get; set; }
        public AppSetting? AppSetting { get; set; }
        public AppSettingOfTableType Type { get; set; }
        public string Value { get; set; } = string.Empty;
        public string ValueJson { get; set; } = string.Empty;
        public Guid ExternalEntityId { get; set; }
    }
}
