﻿using Primitive.AppSetting;

namespace Core.Entity.Setting
{
    public class AppSetting : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public AppSettingType Type { get; set; }
        public List<AppSettingOfTable> AppSettingOfTableList { get; set; } = new List<AppSettingOfTable>();
    }
}
