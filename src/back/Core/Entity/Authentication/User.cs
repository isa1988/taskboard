﻿using Core.Entity.AreaProject;
using Core.Entity.AreaProject.Issue;
using Primitive;

namespace Core.Entity.Authentication
{
    public class User :  IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string FirstName { get; set; } = string.Empty;
        public string MiddleName { get; set; } = string.Empty;
        public string LastName { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public string Login { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public Deleted IsDeleted { get; set; }
        public List<UserRole> RoleList { get; set; } = new List<UserRole>();
        public List<Session> SessionList { get; set; } = new List<Session>();
        public List<ProjectUser> ProjectUserList { get; set; } = new List<ProjectUser>();
        public List<ProjectTaskComment> ProjectTaskCommentList { get; set;} = new List<ProjectTaskComment>();
        public List<ProjectTaskUser> ProjectTaskUserList { get; set; } = new List<ProjectTaskUser>();
    }
}
