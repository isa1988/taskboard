﻿using Primitive;

namespace Core.Entity.Authentication
{
    public class Role : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public Deleted IsDeleted { get; set; }
        public List<UserRole> UserRoleList { get; set; } = new List<UserRole>();
    }
}
