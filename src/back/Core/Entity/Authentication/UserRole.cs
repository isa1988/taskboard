﻿namespace Core.Entity.Authentication
{
    public class UserRole : IEntity
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid RoleId { get; set; }
        public Role? Role { get; set; }
        public Guid UserId { get; set; }
        public User? User { get; set; }
    }
}
