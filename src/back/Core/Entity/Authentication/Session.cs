﻿
using Primitive.Auth;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Entity.Authentication
{
    public class Session : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid UserId { get; set; }
        public User? User { get; set; }
        public Guid? RoleId { get; set; }
        [ForeignKey(nameof(RoleId))]
        public Role? Role { get; set; }
        public string Token { get; set; } = string.Empty;
        public SessionStatus Status { get; set; }
        public DateTime CloseDate { get; set; }
    }
}
