﻿using Core.Entity.AreaProject.Issue;
using Primitive;

namespace Core.Entity.AreaProject
{
    public class ProjectType : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public Guid ProjectId { get; set; }
        public Project? Project { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<ProjectTask> ProjectTaskList { get; set; } = new List<ProjectTask>();
    }
}
