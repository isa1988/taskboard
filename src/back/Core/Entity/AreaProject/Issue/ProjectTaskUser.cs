﻿using Core.Entity.Authentication;
using Primitive.Project;

namespace Core.Entity.AreaProject.Issue
{
    public class ProjectTaskUser : IEntity
    {
        public Guid ProjectTaskId { get; set; }
        public ProjectTask? ProjectTask { get; set; }
        public Guid UserId { get; set; }
        public User? User { get; set; }
        public ProjectTaskUserType Type { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
