﻿using Core.Entity.Setting;

namespace Core.Entity.AreaProject.Issue
{
    public class ProjectTaskCommentFile : IEntity
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public ProjectTaskComment? ProjectTaskComment { get; set; }
        public Guid ProjectTaskCommentId { get; set; }
        public Guid FileAreaId { get; set; }
        public FileArea? FileArea { get; set; }
    }
}
