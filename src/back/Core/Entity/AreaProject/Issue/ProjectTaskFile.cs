﻿using Core.Entity.Setting;

namespace Core.Entity.AreaProject.Issue
{
    public class ProjectTaskFile : IEntity
    {
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public ProjectTask? ProjectTask { get; set; }
        public Guid ProjectTaskId { get; set; }
        public Guid FileAreaId { get; set; }
        public FileArea? FileArea { get; set; }
    }
}
