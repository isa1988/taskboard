﻿
using Core.Entity.Authentication;

namespace Core.Entity.AreaProject.Issue
{
    public class ProjectTaskComment : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public Guid ProjectTaskId { get; set; }
        public ProjectTask? ProjectTask { get; set; }
        public string Name { get; set; } = string.Empty;
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Guid UserId { get; set; }
        public User? User { get; set; }
        public List<ProjectTaskCommentFile> ProjectTaskCommentFileList { get; set; } = new List<ProjectTaskCommentFile>();
    }
}
