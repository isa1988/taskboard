﻿using Primitive;

namespace Core.Entity.AreaProject.Issue
{
    public class ProjectTask : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public long Number { get; set; }
        public string Description { get; set; } = string.Empty;
        public Guid ProjectId { get; set; }
        public Project? Project { get; set; }
        public Guid ProjectTypeId { get; set; }
        public ProjectType? ProjectType { get; set; }
        public Guid ProjectStatusId { get; set; }
        public ProjectStatus? ProjectStatus { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<ProjectTaskComment> ProjectTaskCommentList { get; set; } = new List<ProjectTaskComment>();
        public List<ProjectTaskFile> ProjectTaskFileList { get; set; } = new List<ProjectTaskFile>();
        public List<ProjectTaskResponsibility> ProjectTaskResponsibilityList { get; set; } = new List<ProjectTaskResponsibility>();
        public List<ProjectTaskUser> ProjectTaskUserList { get; set; } = new List<ProjectTaskUser>();
    }
}
