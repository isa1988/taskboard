﻿
namespace Core.Entity.AreaProject.Issue
{
    public class ProjectTaskResponsibility : IEntity
    {
        public Guid ProjectResponsibilityId { get; set; }
        public ProjectResponsibility? ProjectResponsibility { get; set; }
        public Guid ProjectTaskId { get; set; }
        public ProjectTask? ProjectTask { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
