﻿using Core.Entity.AreaProject.Issue;
using Primitive;

namespace Core.Entity.AreaProject
{
    public class ProjectStatus : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public Project? Project { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int Order { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<ProjectStatusSetting> ProjectStatusSettingList { get; set; } = new List<ProjectStatusSetting>();
        public List<ProjectTask> ProjectTaskList { get; set; } = new List<ProjectTask>();
    }
}
