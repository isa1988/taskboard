﻿using Primitive.Project;

namespace Core.Entity.AreaProject
{
    public class ProjectStatusSetting : IEntity
    {
        public Guid ProjectStatusId { get; set; }
        public ProjectStatus? ProjectStatus { get; set; }
        public ProjectStatusSettingEnum Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
