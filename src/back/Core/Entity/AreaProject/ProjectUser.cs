﻿using Core.Entity.Authentication;

namespace Core.Entity.AreaProject
{
    public class ProjectUser : IEntity
    {
        public Guid ProjectId { get; set; }
        public Project? Project { get; set; }
        public Guid UserId { get; set; }
        public User? User { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
