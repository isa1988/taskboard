﻿using Core.Entity.AreaProject.Issue;
using Primitive;

namespace Core.Entity.AreaProject
{
    public class Project : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Prefix { get; set; } = string.Empty;
        public Deleted IsDeleted { get; set; }
        public List<ProjectTask> ProjectTaskList { get; set; } = new List<ProjectTask>();
        public List<ProjectStatus> ProjectStatusList { get; set; } = new List<ProjectStatus>();
        public List<ProjectType> ProjectTypeList { get; set; } = new List<ProjectType>();
        public List<ProjectResponsibility> ProjectResponsibilityList { get; set; } = new List<ProjectResponsibility>();
        public List<ProjectUser> ProjectUserList { get; set; } = new List<ProjectUser>();
    }
}
