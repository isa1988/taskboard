﻿using Core.Entity.AreaProject.Issue;
using Primitive;

namespace Core.Entity.AreaProject
{
    public class ProjectResponsibility : IEntityForDelete<Guid>
    {
        public Guid Id { get; set; }
        public Guid ProjectId { get; set; }
        public Project? Project { get; set; }
        public string Name { get; set; } = string.Empty;
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Deleted IsDeleted { get; set; }
        public List<ProjectTaskResponsibility> ProjectTaskResponsibilityList { get; set; } = new List<ProjectTaskResponsibility>();
    }
}
