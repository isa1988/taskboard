﻿using Primitive.PagingOrderSettings;

namespace Core.Helper
{
    public record PagingOrderSetting<T>
        where T : Enum
    {
        public PagingPageSetting? PageSetting { get; set; }
        public bool IsOrder { get; set; }
        public T OrderField { get; set; }
        public OrderType OrderDirection { get; set; }
    }
}
