﻿namespace Core.Helper
{
    public record ResolveOptions
    {
        public bool IsUser { get; set; }
        public bool IsRole { get; set; }
        public bool IsSessions { get; set; }
        public bool IsProject { get; set; }
        public bool IsProjectResponsibility { get; set; }
        public bool IsProjectStatus { get; set; }
        public bool IsProjectType { get; set; }
        public bool ProjectTask { get; set; }
        public bool AppSetting { get; set; }
        public bool FileArea { get; set; }

        public bool IsRoleList { get; set; }
        public bool IsProjectStatusSettingList { get; set; }
        public bool IsProjectUserList { get; set; }
        public bool ProjectTaskCommentList { get; set; }
        public bool ProjectTaskCommentFileList { get; set; }
        public bool ProjectTaskFileList { get; set; }
        public bool IsProjectTaskResponsibilityList { get; set; }
        public bool IsProjectTaskUserList { get; set; }
        public bool IsAppSettingOfTableList { get; set; }
    }
}
