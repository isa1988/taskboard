﻿namespace Primitive.PagingOrderSettings
{
    public enum OrderType
    {
        ASC,
        DESC
    }
}
