﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Project
{
    public enum OrderProjectStatusFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По наименованию")]
        Name,

        [Display(Name = "По шагу")]
        Order,
    }
}
