﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Project
{
    public enum OrderProjectResponsibilityFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По имени")]
        Name
    }
}
