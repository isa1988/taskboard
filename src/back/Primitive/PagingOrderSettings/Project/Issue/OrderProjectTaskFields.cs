﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Project.Issue
{
    public enum OrderProjectTaskFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По наименованию")]
        Name
    }
}
