﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Project.Issue
{
    public enum OrderProjectTaskCommentFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По наименованию")]
        Name
    }
}
