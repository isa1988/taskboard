﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Project
{
    public enum OrderProjectFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По наименованию")]
        Name
    }
}
