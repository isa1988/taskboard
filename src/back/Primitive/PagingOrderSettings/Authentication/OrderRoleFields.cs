﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Authentication
{
    public enum OrderRoleFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По имени")]
        Name
    }
}
