﻿using System.ComponentModel.DataAnnotations;


namespace Primitive.PagingOrderSettings.Authentication
{
    public enum OrderUserFields
    {
        [Display(Name = "Нет сортировки")]
        None,

        [Display(Name = "По имени")]
        Name,

        [Display(Name = "По телефону")]
        Phone,

        [Display(Name = "По эл. почте")]
        Email
    }
}
