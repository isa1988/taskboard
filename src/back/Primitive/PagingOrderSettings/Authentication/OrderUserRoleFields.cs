﻿using System.ComponentModel.DataAnnotations;

namespace Primitive.PagingOrderSettings.Authentication
{
    public enum OrderUserRoleFields
    {
        [Display(Name = "Нет сортировки")]
        None,
        [Display(Name = "По ID пользовтаеля")]
        UserId
    }
}
