﻿namespace Primitive.Auth
{
    public enum SessionStatus : int
    {
        Active = 0,
        Closed = 1
    }
}
