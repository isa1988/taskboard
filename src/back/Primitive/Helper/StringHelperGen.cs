﻿namespace Primitive.Helper
{
    public static class StringHelperGen
    {
        public static string? TrimWithNull(this string? value)
        {
            if (value == null || value == string.Empty)
            {
                return value;
            }
            return value.Trim();
        }
    }
}
