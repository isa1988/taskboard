using WebAPI.AppStart;

var builder = WebApplication.CreateBuilder(args);

var conf = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");

var env = builder.Environment;
if (env.IsDevelopment())
{
    conf = new ConfigurationBuilder()
        .AddJsonFile("appsettings.Development.json");
}
var configuration = conf.Build();

builder.Services.AddDatabaseContext(configuration);

// Add services to the container.

builder.Services.AddControllers();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
