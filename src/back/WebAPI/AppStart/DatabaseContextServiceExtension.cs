﻿using DAL.Data;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.AppStart
{
    public static class DatabaseContextServiceExtension
    {
        public static void AddDatabaseContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connection = configuration.GetConnectionString("DefaultConnection");

            services.AddDbContextPool<TaskBoardContext>(options => options.UseSqlServer(connection));
        }
    }
}
